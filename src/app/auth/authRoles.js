/**
 * Authorization Roles
 */
const authRoles = {
    //admin    : ['admin'],
    //staff    : ['admin', 'staff'],
    //user     : ['admin', 'staff', 'user'],
    onlyGuest: [],

    superadmin      : ['super_admin'],
    admin           : ['admin'],
    yonetici        : ['yonetici'],
    satis_yonetici  : ['satis_yonetici'],
    teknik_yonetici : ['teknik_yonetici'],
    mali_yonetici   : ['mali_yonetici'],
    satis_personel  : ['satis_personel'],
    teknik_personel : ['teknik_personel'],
    mali_personel   : ['mali_personel'],
    musteri         : ['musteri'],

/*
    superadmin      : ['superadmin'],                                                               // biz
    admin           : ['superadmin', 'admin'],                                                      // sistemi konfigure edecek kisi\ alpaslan
    yonetici        : ['superadmin', 'admin', 'yonetici'],                                          // is yoneticisi Julide hn
    satis_yonetici  : ['superadmin', 'admin', 'yonetici', 'satis_yonetici'  ],                      // Satis grubu yoneticisi
    teknik_yonetici : ['superadmin', 'admin', 'yonetici', 'teknik_yonetici' ],                      // Teknik yonetici
    mali_yonetici   : ['superadmin', 'admin', 'yonetici', 'mali_yonetici'   ],
    satis_personel  : ['superadmin', 'admin', 'yonetici', 'satis_yonetici',  'satis_personel'],
    teknik_personel : ['superadmin', 'admin', 'yonetici', 'teknik_yonetici', 'teknik_personel'],
    mali_personel   : ['superadmin', 'admin', 'yonetici', 'mali_yonetici',   'mali_personel'],
    musteri         : ['superadmin', 'admin', 'musteri']                                            // Disaridaki musterilerden kullanici
*/
};

export default authRoles;
