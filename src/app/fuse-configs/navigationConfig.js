import i18next from 'i18next';
import en from './navigation-i18n/en';
import tr from './navigation-i18n/tr';
import ar from './navigation-i18n/ar';
import { authRoles } from 'app/auth';
import _ from '@lodash';

i18next.addResourceBundle('en', 'navigation', en);
i18next.addResourceBundle('tr', 'navigation', tr);
i18next.addResourceBundle('ar', 'navigation', ar);

const navigationConfig = [
    {
        'id'       : 'satis-dashboard-component',
        'title'    : 'Satış Masası',
        'translate': 'Satış Masası',
        'type'     : 'item',
        'icon'     : 'whatshot',
        'auth'     : authRoles.satis_personel,
        'url'      : '/dashboards/satis'
    },
    {
        'id'       : 'satis-dashboard-component-yonetim',
        'title'    : 'Satış Yönetim Masası',
        'translate': 'Satış Yönetim Masası',
        'type'     : 'item',
        'icon'     : 'whatshot',
        'auth'     : authRoles.satis_yonetici,
        'url'      : '/dashboards/satis/yonetim'
    },
    {
        'id'       : 'teknik-dashboard-component',
        'title'    : 'Ana Sayfa',
        'translate': 'Ana Sayfa',
        'type'     : 'item',
        'icon'     : 'whatshot',
        'auth'     : authRoles.teknik_personel,
        'url'      : '/dashboards/teknik'
    },
    {
        'id'       : 'teknik-dashboard-component-yonetim',
        'title'    : 'Teknik Yönetim Masası',
        'translate': 'Teknik Yönetim Masası',
        'type'     : 'item',
        'icon'     : 'whatshot',
        'auth'     : authRoles.teknik_yonetici,
        'url'      : '/dashboards/teknik/yonetim'
    },
    {
        'type': 'divider',
        'id'  : 'divider-1'
    },
    {
        'id'       : 'musteriler-1',
        'title'    : 'Müşteriler',
        'translate': 'Müşteriler',
        'type'     : 'item',
        'icon'     : 'whatshot',
        'auth'     : _.union(authRoles.satis_yonetici, authRoles.satis_personel),
        'url'      : '/musteriler'
    },
    {
        'id'       : 'urunler',
        'title'    : 'Ürünler',
        'translate': 'Ürünler',
        'type'     : 'item',
        'icon'     : 'account_box',
        'auth'     : _.union(authRoles.satis_yonetici, authRoles.satis_personel),
        'url'      : '/urunler/all'
    },
    {
        'id'       : 'contacts',
        'title'    : 'Kişiler',
        'translate': 'Kişiler',
        'type'     : 'item',
        'icon'     : 'account_box',
        'url'      : '/kisiler/all',
        'auth'     : _.union(authRoles.satis_yonetici, authRoles.satis_personel),
    },
    {
        'id'       : 'teklifler',
        'title'    : 'Teklifler',
        'translate': 'Teklifler',
        'type'     : 'item',
        'icon'     : 'account_box',
        'url'      : '/teklifler/all',
        'auth'     : _.union(authRoles.satis_personel, authRoles.satis_yonetici)
    },
    {
        'id'       : 'admin',
        'title'    : 'Admin',
        'translate': 'Admin',
        'type'     : 'group',
        'icon'     : 'account_box',
        'auth'     : _.union(authRoles.admin),
        'children'  : [
            {
                'id'       : 'admin-users',
                'title'    : 'Kullanicilar',
                'translate': 'Kullanicilar',
                'type'     : 'item',
                'icon'     : 'account_box',
                'url'      : '/admin/users',
            },
        ]
    },

];

export default navigationConfig;
