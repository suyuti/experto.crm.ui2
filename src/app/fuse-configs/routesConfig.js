import React from 'react';
import {Redirect} from 'react-router-dom';
import {FuseUtils} from '@fuse';
import {ExampleConfig} from 'app/main/example/ExampleConfig';
import {LoginConfig} from 'app/main/login/LoginConfig'
import { MusteriAppConfig } from 'app/main/musteriApp/MusteriAppConfig';
import { SatisDashboardConfig } from 'app/main/dashboards/satis/SatisDashboardConfig';
import { SatisYonetimDashboardConfig } from 'app/main/dashboards/satis/SatisYonetimDashboardConfig';
import { TeknikDashboardConfig } from 'app/main/dashboards/teknik/TeknikDashboardConfig';
import { TeknikYonetimDashboardConfig } from 'app/main/dashboards/teknik/TeknikYonetimDashboardConfig';
import { RegisterConfig } from 'app/main/register/RegisterConfig';
import { ContactsAppConfig} from 'app/main/kisilerApp/ContactsAppConfig'
import { TeklifAppConfig } from 'app/main/teklifApp/TeklifAppConfig'
import { UrunAppConfig } from 'app/main/urunApp/UrunAppConfig'
import { AdminAppConfig } from 'app/main/adminApp/AdminAppConfig'
const routeConfigs = [
    //ExampleConfig,

    LoginConfig,
    RegisterConfig,
    
    MusteriAppConfig,
    
    SatisYonetimDashboardConfig,
    SatisDashboardConfig,
    TeknikYonetimDashboardConfig,
    TeknikDashboardConfig,
    
    ContactsAppConfig,
    TeklifAppConfig,
    UrunAppConfig,
    
    AdminAppConfig
];


const routes = [
    ...FuseUtils.generateRoutesFromConfigs(routeConfigs, 
        ['superadmin', 'admin', 'yonetici', 'satis_yonetici',  'satis_personel', 'teknik_personel', 'mali_personel', 'musteri']
        //['admin', 'staff', 'user']
    ),
    {
        path     : '/',
        exact : true,
        component: () => <Redirect to="/"/>
    }
];

export default routes;
