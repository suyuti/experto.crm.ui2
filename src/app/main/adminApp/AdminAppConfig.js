import React from 'react';


export const AdminAppConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path     : '/admin/dashboard',
            component: React.lazy(() => import('./dashboard/AdminDashboard'))
        },
        {
            path     : '/admin/users',
            component: React.lazy(() => import('./users/Users'))
        },
    ]
};

