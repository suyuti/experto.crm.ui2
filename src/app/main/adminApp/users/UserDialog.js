import React, { useEffect, useCallback } from 'react';
import { TextField, Select, MenuItem, OutlinedInput, Button, Dialog, DialogActions, DialogContent, Icon, IconButton, Typography, Toolbar, AppBar, Avatar } from '@material-ui/core';
import { useForm } from '@fuse/hooks';
import { FuseLoading } from '@fuse';
import FuseUtils from '@fuse/FuseUtils';
import * as Actions from './store/actions';
import { useDispatch, useSelector } from 'react-redux';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

const {authRoles} = require('app/auth/authRoles')

const defaultFormState = {
    id: '',
    active: '',
    role: '',
};

function UserDialog(props) {
    const dispatch = useDispatch();
    const userDialog = useSelector(({ usersApp }) => usersApp.users.userDialog);

    const roles = [
        'super_admin',
        'admin',
        'yonetici',
        'satis_yonetici',
        'teknik_yonetici',
        'mali_yonetici',
        'satis_personel',
        'teknik_personel',
        'mali_personel',
        'musteri',
        ] // TODO authRoles.js den alinmali

    const { form, handleChange, setForm } = useForm(defaultFormState);

    const initDialog = useCallback(
        () => {
            /**
             * Dialog type: 'edit'
             */
            if (userDialog.type === 'edit' && userDialog.data) {
                setForm({ ...userDialog.data });
            }

            /**
             * Dialog type: 'new'
             */
            if (userDialog.type === 'new') {
                setForm({
                    ...defaultFormState,
                    ...userDialog.data,
                    id: FuseUtils.generateGUID()
                });
            }
        },
        [userDialog.data, userDialog.type, setForm],
    );

    useEffect(() => {
        /**
         * After Dialog Open
         */

        if (userDialog.props.open) {
            initDialog();
        }

    }, [userDialog.props.open, initDialog]);

    function closeComposeDialog() {
        userDialog.type === 'edit' ? dispatch(Actions.closeEditUserDialog()) : dispatch(Actions.closeNewUserDialog());
    }

    function canBeSubmitted() {
        return (
            true
            //form.Adi.length > 0
        );
    }

    function handleSubmit(event) {
        event.preventDefault();

        if (userDialog.type === 'new') {
            //dispatch(Actions.addUser(form));
        }
        else {
            dispatch(Actions.updateUserByAdmin(form));
        }
        closeComposeDialog();
    }

    function handleRemove() {
        dispatch(Actions.removeUser(form.id));
        closeComposeDialog();
    }

    return (
        <Dialog
            classes={{
                paper: "m-24"
            }}
            {...userDialog.props}
            onClose={closeComposeDialog}
            fullWidth
            maxWidth="xs"
        >

            <AppBar position="static" elevation={1}>
                <Toolbar className="flex w-full">
                    <Typography variant="subtitle1" color="inherit">
                        {userDialog.type === 'new' ? 'Yeni Kişi' : 'Kişi Güncelle'}
                    </Typography>
                </Toolbar>
                <div className="flex flex-col items-center justify-center pb-24">
                    <Avatar className="w-96 h-96" alt="contact avatar" src={form.avatar} />
                    {userDialog.type === 'edit' && (
                        <Typography variant="h6" color="inherit" className="pt-8">
                            {form.Adi}
                        </Typography>
                    )}
                </div>
            </AppBar>
            <form noValidate onSubmit={handleSubmit} className="flex flex-col md:overflow-hidden">
                <DialogContent classes={{ root: "p-24" }}>
                    <div className="flex">
                        <FormControlLabel
                            value="start"
                            control={<Switch
                                name= "active"
                                checked={form.active}
                                onChange={handleChange}
                                value={form.active}
                                inputProps={{ 'aria-label': 'secondary checkbox' }}
                            />
                            }
                            label="Aktif"
                            labelPlacement="start"
                        />
                    </div>
                    <div className="flex">
                    <Select
                        fullWidth
                        required
                        labelId="sektor-select-outlined-label"
                        name="role"
                        className="mt-8 mb-16"
                        value={form.role}
                        onChange={handleChange}
                        input={
                            <OutlinedInput
                                //labelWidth={("category".length * 9)}
                                //name="musteri"
                                id="firma"
                            />
                        }
                    >
                        {
                            roles.map(role => (
                                <MenuItem value={role} key={role}>{role}</MenuItem>
                            ))
                            }
                    </Select>
                    </div>

                </DialogContent>

                {userDialog.type === 'new' ? (
                    <DialogActions className="justify-between p-8">
                        <div className="px-16">
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={handleSubmit}
                                type="submit"
                                disabled={!canBeSubmitted()}
                            >
                                Ekle
                            </Button>
                        </div>
                    </DialogActions>
                ) : (
                        <DialogActions className="justify-between p-8">
                            <div className="px-16">
                            <Button
                                    variant="contained"
                                    color="primary"
                                    type="submit"
                                    onClick={handleSubmit}
                                    disabled={!canBeSubmitted()}
                                >
                                    Kaydet
                                </Button>
                            </div>
                            <IconButton
                                onClick={handleRemove}
                            >
                                <Icon>delete</Icon>
                            </IconButton>
                        </DialogActions>
                    )}
            </form>
        </Dialog>
    );
}

export default UserDialog;
