import React, {useEffect, useState} from 'react';
import {Avatar, Checkbox, Icon, IconButton, Typography} from '@material-ui/core';
import {FuseUtils, FuseAnimate} from '@fuse';
import {useDispatch, useSelector} from 'react-redux';
import ReactTable from "react-table";
import * as Actions from './store/actions';
import UsersMultiSelectMenu from './UsersMultiSelectMenu';
import { stringify } from 'qs';

function UsersList(props)
{
    const dispatch          = useDispatch();
    const users             = useSelector(({usersApp}) => usersApp.users.entities);
    const selectedUserIds   = useSelector(({usersApp}) => usersApp.users.selectedUserIds);
    const searchText        = useSelector(({usersApp}) => usersApp.users.searchText);
    const user              = useSelector(({usersApp}) => usersApp.user);

    const [filteredData, setFilteredData] = useState(null);

    useEffect(() => {
        function getFilteredArray(entities, searchText)
        {
            const arr = Object.keys(entities).map((id) => entities[id]);
            if ( searchText.length === 0 )
            {
                return arr;
            }
            return FuseUtils.filterArrayByString(arr, searchText);
        }

        if ( users )
        {
            setFilteredData(getFilteredArray(users, searchText));
        }
    }, [users, searchText]);


    if ( !filteredData )
    {
        return null;
    }

    if ( filteredData.length === 0 )
    {
        return (
            <div className="flex flex-1 items-center justify-center h-full">
                <Typography color="textSecondary" variant="h5">
                    Kayıt yok!
                </Typography>
            </div>
        );
    }

    return (
        <FuseAnimate animation="transition.slideUpIn" delay={300}>
            <ReactTable
                className="-striped -highlight h-full sm:rounded-16 overflow-hidden"
                getTrProps={(state, rowInfo, column) => {
                    return {
                        className: "cursor-pointer",
                        onClick  : (e, handleOriginal) => {
                            if ( rowInfo )
                            {
                                dispatch(Actions.openEditUserDialog(
                                    {
                                        id: rowInfo.original._id,
                                        displaName: rowInfo.original.displayName, 
                                        active: rowInfo.original.active,
                                        role: rowInfo.original.role
                                    }));
                            }
                        }
                    }
                }}
                data={filteredData}
                columns={[
                    {
                        Header   : () => (
                            <Checkbox
                                onClick={(event) => {
                                    event.stopPropagation();
                                }}
                                onChange={(event) => {
                                    event.target.checked ? dispatch(Actions.selectAllUsers()) : dispatch(Actions.deSelectAllUsers());
                                }}
                                checked={selectedUserIds.length === Object.keys(users).length && selectedUserIds.length > 0}
                                indeterminate={selectedUserIds.length !== Object.keys(users).length && selectedUserIds.length > 0}
                            />
                        ),
                        accessor : "",
                        Cell     : row => {
                            return (<Checkbox
                                    onClick={(event) => {
                                        event.stopPropagation();
                                    }}
                                    checked={selectedUserIds.includes(row.value._id)}
                                    onChange={() => dispatch(Actions.toggleInSelectedUsers(row.value._id))}
                                />
                            )
                        },
                        className: "justify-center",
                        sortable : false,
                        width    : 64
                    },
                    {
                        Header   : () => (
                            selectedUserIds.length > 0 && (
                                <UsersMultiSelectMenu/>
                            )
                        ),
                        accessor : "avatar",
                        Cell     : row => (
                            <Avatar className="mx-8" alt={row.original.name} src={row.value}/>
                        ),
                        className: "justify-center",
                        width    : 64,
                        sortable : false
                    },
                    {
                        Header    : "Adi",
                        accessor  : "Adi",
                        filterable: true,
                        className : "font-bold"
                    },
                    {
                        Header    : "Email",
                        accessor  : "Email",
                        //filterable: true
                    },
                    {
                        Header    : "Rol",
                        accessor  : "role",
                        //filterable: true
                    },
                    {
                        id: 'active',
                        Header    : "Aktif",
                        accessor  : d=> {return d.active?"Aktif":"Aktif değil"}
                        //filterable: true
                    },
                    {
                        Header: "",
                        width : 128,
                        Cell  : row => (
                            <div className="flex items-center">
                                <IconButton
                                    onClick={(ev) => {
                                        ev.stopPropagation();
                                        dispatch(Actions.removeUser(row.original._id));
                                    }}
                                >
                                    <Icon>delete</Icon>
                                </IconButton>
                            </div>
                        )
                    }
                ]}
                defaultPageSize={10}
                noDataText="No users found"
            />
        </FuseAnimate>
    );
}

export default UsersList;
