import axios from 'axios';
import {getUserData} from 'app/main/kisilerApp/store/actions/user.actions';

export const GET_USERS                  = '[USERS APP] GET USERS';
export const SET_SEARCH_TEXT            = '[USERS APP] SET SEARCH TEXT';
export const TOGGLE_IN_SELECTED_USERS   = '[USERS APP] TOGGLE IN SELECTED USERS';
export const SELECT_ALL_USERS           = '[USERS APP] SELECT ALL USERS';
export const DESELECT_ALL_USERS         = '[USERS APP] DESELECT ALL USERS';
export const OPEN_NEW_USER_DIALOG       = '[USERS APP] OPEN NEW USER DIALOG';
export const CLOSE_NEW_USER_DIALOG      = '[USERS APP] CLOSE NEW USER DIALOG';
export const OPEN_EDIT_USER_DIALOG      = '[USERS APP] OPEN EDIT USER DIALOG';
export const CLOSE_EDIT_USER_DIALOG     = '[USERS APP] CLOSE EDIT USER DIALOG';
export const ADD_USER                   = '[USERS APP] ADD USER';
export const UPDATE_USER                = '[USERS APP] UPDATE USER';
export const REMOVE_USER                = '[USERS APP] REMOVE USER';
export const REMOVE_USERS               = '[USERS APP] REMOVE USERS';
export const TOGGLE_STARRED_USER        = '[USERS APP] TOGGLE STARRED USER';
export const TOGGLE_STARRED_USERS       = '[USERS APP] TOGGLE STARRED USERS';
export const SET_USERS_STARRED          = '[USERS APP] SET USERS STARRED ';
export const UPDATE_USER_BY_ADMIN       = '[USERS APP] UPDATE USER BY ADMIN ';  // Role ve Active

export function getUsers(routeParams)
{
    const request = axios.get('/api/user', {
        params: routeParams
    });

    return (dispatch) =>
        request.then((response) => {
            dispatch({
                type   : GET_USERS,
                payload: response.data,
                routeParams
            })}
        );
}

export function setSearchText(event)
{
    return {
        type      : SET_SEARCH_TEXT,
        searchText: event.target.value
    }
}

export function toggleInSelectedUsers(userId)
{
    return {
        type: TOGGLE_IN_SELECTED_USERS,
        userId
    }
}

export function selectAllUsers()
{
    return {
        type: SELECT_ALL_USERS
    }
}

export function deSelectAllUsers()
{
    return {
        type: DESELECT_ALL_USERS
    }
}

export function openNewUserDialog()
{
    return {
        type: OPEN_NEW_USER_DIALOG
    }
}

export function closeNewUserDialog()
{
    return {
        type: CLOSE_NEW_USER_DIALOG
    }
}

export function openEditUserDialog(data)
{
    return {
        type: OPEN_EDIT_USER_DIALOG,
        data
    }
}

export function closeEditUserDialog()
{
    return {
        type: CLOSE_EDIT_USER_DIALOG
    }
}

export function addUSER(newUser)
{
    return (dispatch, getState) => {

        const {routeParams} = getState().UsersApp.Users;

        const request = axios.post('/api/kisi', 
            newUser
        );

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: ADD_USER
                })
            ]).then(() => dispatch(getUsers(routeParams)))
        );
    };
}

export function updateUser(user)
{
    return (dispatch, getState) => {

        const {routeParams} = getState().UsersApp.Users;

        const request = axios.post('/api/USERS-app/update-USER', {
            user
        });

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: UPDATE_USER
                })
            ]).then(() => dispatch(getUsers(routeParams)))
        );
    };
}

export function removeUser(userId)
{
    return (dispatch, getState) => {

        const {routeParams} = getState().usersApp.users;

        const request = axios.delete('/api/user/'+userId);

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: REMOVE_USER
                })
            ]).then(() => dispatch(getUsers(routeParams)))
        );
    };
}


export function removeUsers(userIds)
{
    return (dispatch, getState) => {

        const {routeParams} = getState().UsersApp.users;

        const request = axios.post('/api/USERS-app/remove-USERS', {
            userIds
        });

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: REMOVE_USERS
                }),
                dispatch({
                    type: DESELECT_ALL_USERS
                })
            ]).then(() => dispatch(getUsers(routeParams)))
        );
    };
}

export function toggleStarredUser(userId)
{
    return (dispatch, getState) => {
        const {routeParams} = getState().UsersApp.Users;

        const request = axios.post('/api/USERS-app/toggle-starred-USER', {
            userId
        });

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: TOGGLE_STARRED_USER
                }),
                dispatch(getUserData())
            ]).then(() => dispatch(getUsers(routeParams)))
        );
    };
}

export function toggleStarredUsers(userIds)
{
    return (dispatch, getState) => {

        const {routeParams} = getState().UsersApp.Users;

        const request = axios.post('/api/USERS-app/toggle-starred-USERS', {
            userIds
        });

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: TOGGLE_STARRED_USERS
                }),
                dispatch({
                    type: DESELECT_ALL_USERS
                }),
                dispatch(getUserData())
            ]).then(() => dispatch(getUsers(routeParams)))
        );
    };
}

export function setUsersStarred(USERIds)
{
    return (dispatch, getState) => {

        const {routeParams} = getState().USERSApp.USERS;

        const request = axios.post('/api/USERS-app/set-USERS-starred', {
            USERIds
        });

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: SET_USERS_STARRED
                }),
                dispatch({
                    type: DESELECT_ALL_USERS
                }),
                dispatch(getUserData())
            ]).then(() => dispatch(getUsers(routeParams)))
        );
    };
}

export function setUSERSUnstarred(USERIds)
{
    return (dispatch, getState) => {

        const {routeParams} = getState().USERSApp.USERS;

        const request = axios.post('/api/USERS-app/set-USERS-unstarred', {
            USERIds
        });

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: SET_USERS_STARRED
                }),
                dispatch({
                    type: DESELECT_ALL_USERS
                }),
                dispatch(getUserData())
            ]).then(() => dispatch(getUsers(routeParams)))
        );
    };
}

export function updateUserByAdmin(user)
    {
        return (dispatch, getState) => {
            const {routeParams} = getState().usersApp.users;
    
            const request = axios.put('/api/user/updateUserByAdmin', {
                user
            });
    
            return request.then((response) =>
                Promise.all([
                    dispatch({
                        type: UPDATE_USER_BY_ADMIN
                    })
                ]).then(() => dispatch(getUsers(routeParams)))
            );
        };
    }
    