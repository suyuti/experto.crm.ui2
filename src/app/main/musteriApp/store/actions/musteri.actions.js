import axios from 'axios';
import {FuseUtils} from '@fuse';
import {showMessage} from 'app/store/actions/fuse';


export const GET_MUSTERI             = '[MUSTERI APP] GET MUSTERILER';
export const SAVE_MUSTERI            = '[MUSTERI APP] SAVE MUSTERILER';
export const CREATE_MUSTERI          = '[MUSTERI APP] CREATE MUSTERILER';


export function getMusteri(params)
{
    //dispatch({type:GET_MUSTERI, payload: {data:null}})
    const request = axios.get('/api/musteri', {params});

    return (dispatch) =>
        request.then((response) => {
            dispatch({
                type   : GET_MUSTERI,
                payload: response.data
            })}
        );
}

export function saveMusteri(data)
{
    const request = axios.put('/api/musteri', data);

    return (dispatch) =>
        request.then((response) => {

                dispatch(showMessage({message: 'Musteri kaydedildi.'}));

                return dispatch({
                    type   : SAVE_MUSTERI,
                    payload: response.data
                })
            }
        );
}

export function createMusteri(data)
{
    const request = axios.post('/api/musteri/save', data);

    return (dispatch) =>
        request.then((response) => {

                dispatch(showMessage({message: 'Musteri kaydedildi.'}));

                return dispatch({
                    type   : SAVE_MUSTERI,
                    payload: response.data
                })
            }
        );
}

export function newMusteri()
{
    const data = {
        id              : FuseUtils.generateGUID(),
        FirmaMarkasi: '',
        FirmaUnvani: '',
        Sektor: '',
        il: '',
        ilce: '',
        Adres: '',
        lokasyon: '',
        Telefon: '',
        OSB: '',
        Web: '',
        Mail: '',
        calisanSayisi: 0,
        muhendisSayisi: 0,
        teknikPersonelSayisi: 0,
        ciro: 0,
        KobiMi: false,
        vergiDairesi: '',
        vergiNo: '',
        temsilci: '',
        logo: '',
        active          : true
    };

    return {
        type   : GET_MUSTERI,
        payload: {data:data}
    }
}

