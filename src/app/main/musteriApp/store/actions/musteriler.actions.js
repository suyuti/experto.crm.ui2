import axios from 'axios';

export const GET_MUSTERILER             = '[MUSTERI] GET MUSTERILER';
export const SELECT_ALL_MUSTERILER      = '[MUSTERI] SELECT ALL MUSTERILER';
export const DESELECT_ALL_MUSTERILER    = '[MUSTERI] DESELECT ALL MUSTERILER';
export const OPEN_NEW_MUSTERI_DIALOG    = '[MUSTERI] OPEN NEW MUSTERI DIALOG';
export const CLOSE_NEW_MUSTERI_DIALOG   = '[MUSTERI] CLOSE NEW MUSTERI DIALOG';
export const SET_SEARCH_TEXT            = '[MUSTERI APP] SET SEARCH TEXT';
export const TOGGLE_IN_SELECTED_MUSTERI = '[MUSTERI APP] TOGGLE IN SELECTED MUSTERI';
export const REMOVE_MUSTERI             = '[MUSTERI APP] REMOVE MUSTERI';

export function getMusteriler(routeParams)
{
    const request = axios.get('/api/musteri', {
        params: routeParams
    });

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_MUSTERILER,
                payload: response.data,
                routeParams
            })
        );
}

export function selectAllMusteriler()
{
    return {
        type: SELECT_ALL_MUSTERILER
    }
}

export function deselectAllMusteriler()
{
    return {
        type: DESELECT_ALL_MUSTERILER
    }
}

export function setSearchText(event)
{
    return {
        type      : SET_SEARCH_TEXT,
        searchText: event.target.value
    }
}

export function toggleInSelectedMusteri(musteriId)
{
    return {
        type: TOGGLE_IN_SELECTED_MUSTERI,
        musteriId
    }
}

export function removeMusteri(musteriId)
{
    return (dispatch, getState) => {

        const {routeParams} = getState().musteriApp.musteriler;

        const request = axios.delete('/api/musteri/' + musteriId);

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: REMOVE_MUSTERI
                })
            ]).then(() => dispatch(getMusteriler(routeParams)))
        );
    };
}


/*
export function openNewMusteriDialog()
{
    return {
        type: OPEN_NEW_MUSTERI_DIALOG
    }
}

export function closeNewMusteriDialog()
{
    return {
        type: CLOSE_NEW_MUSTERI_DIALOG
    }
}
*/