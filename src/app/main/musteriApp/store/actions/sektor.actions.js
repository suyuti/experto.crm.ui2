import axios from 'axios';

export const GET_SEKTORLER             = '[MUSTERI] GET SEKTORLER';

export function getSektorler(routeParams)
{
    const request = axios.get('/api/sektor', {
        params: routeParams
    });

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_SEKTORLER,
                payload: response.data,
                routeParams
            })
        );
}
