import * as Actions from '../actions';
import _ from '@lodash';

const initialState = {
    entities: null,
    searchText: '',
    selectedMusteriIds: [],
    routeParams: {},
    musteriDialog: {
        type: 'new',
        props: {
            open: false
        },
        data: null
    }
};

const musterilerReducer = function (state = initialState, action) {
    switch (action.type) {
        case Actions.GET_MUSTERILER:
            {
                console.log(action.payload)
                console.log(_.keyBy(action.payload.data, '_id'))
                return {
                    ...state,
                    entities: _.keyBy(action.payload.data, '_id'),
                    routeParams: action.routeParams
                };
            }
        case Actions.SELECT_ALL_MUSTERILER:
            {
                const arr = Object.keys(state.entities).map(k => state.entities[k]);

                const selectedMusteriIds = arr.map(musteri => musteri._id);

                return {
                    ...state,
                    selectedMusteriIds: selectedMusteriIds
                };
            }
        case Actions.DESELECT_ALL_MUSTERILER:
            {
                return {
                    ...state,
                    selectedMusteriIds: []
                };
            }
        case Actions.OPEN_NEW_MUSTERI_DIALOG:
            {
                return {
                    ...state,
                    musteriDialog: {
                        type: 'new',
                        props: {
                            open: true
                        },
                        data: null
                    }
                };
            }
        case Actions.CLOSE_NEW_MUSTERI_DIALOG:
            {
                return {
                    ...state,
                    musteriDialog: {
                        type: 'new',
                        props: {
                            open: false
                        },
                        data: null
                    }
                };
            }
        case Actions.SET_SEARCH_TEXT:
            {
                return {
                    ...state,
                    searchText: action.searchText
                };
            }
        case Actions.TOGGLE_IN_SELECTED_MUSTERI:
            {

                const musteriId = action.musteriId;

                let selectedMusteriIds = [...state.selectedMusteriIds];

                if (selectedMusteriIds.find(id => id === musteriId) !== undefined) {
                    selectedMusteriIds = selectedMusteriIds.filter(id => id !== musteriId);
                }
                else {
                    selectedMusteriIds = [...selectedMusteriIds, musteriId];
                }

                return {
                    ...state,
                    selectedMusteriIds: selectedMusteriIds
                };
            }


        default:
            {
                return state;
            }
    }
};


export default musterilerReducer;
