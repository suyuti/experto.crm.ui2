import * as Actions from '../actions';

const initialState = {
    data: null
};

const sektorReducer = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.GET_SEKTORLER:
        {
            console.log(action.payload)
            return {
                ...state,
                data: action.payload.data
            };
        }
        default:
        {
            return state;
        }
    }
};

export default sektorReducer;
