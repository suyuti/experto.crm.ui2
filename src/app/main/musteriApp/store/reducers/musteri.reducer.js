import * as Actions from '../actions';

const initialState = {
    data: null
};

const musteriReducer = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.GET_MUSTERI:
        {
            return {
                ...state,
                data: action.payload.data
            };
        }
        case Actions.SAVE_MUSTERI:
        {
            return {
                ...state,
                data: action.payload.data
            };
        }
        default:
        {
            return state;
        }
    }
};

export default musteriReducer;
