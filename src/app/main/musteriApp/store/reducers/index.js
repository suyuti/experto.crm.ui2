import {combineReducers} from 'redux';
import contacts from './contacts.reducer';
import user from './user.reducer';
import musteri from './musteri.reducer';
import musteriler from './musteriler.reducer';
import sektor from './sektor.reducer';

const reducer = combineReducers({
    contacts,
    user,
    musteri,
    musteriler,
    sektor
});

export default reducer;
