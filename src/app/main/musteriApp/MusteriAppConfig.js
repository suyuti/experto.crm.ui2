import React from 'react';


export const MusteriAppConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path     : '/musteriler',
            component: React.lazy(() => import('./musteriler/Musteriler'))
        },
        {
            path     : '/musteriApp/musteri/:musteriId/:musteriHandle?',
            component: React.lazy(() => import('./musteri/Musteri'))
        }
    ]
};

