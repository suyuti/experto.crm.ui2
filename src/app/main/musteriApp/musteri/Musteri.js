import React, { useEffect, useState } from 'react';
import {
    Button,
    Tab,
    Tabs,
    TextField,
    OutlinedInput,
    InputAdornment,
    Icon,
    Card,
    InputLabel,
    Select,
    MenuItem,
    Typography,
    Checkbox,
    FormControlLabel
} from '@material-ui/core';
import { useTheme } from '@material-ui/styles';
import { FuseAnimate, FusePageCarded, FuseLoading } from '@fuse';
import { useForm } from '@fuse/hooks';
import { Link } from 'react-router-dom';
import _ from '@lodash';
import { useDispatch, useSelector } from 'react-redux';
import withReducer from 'app/store/withReducer';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import GoogleMap from 'google-map-react';
var iller = require('il-ilce.json')

/*
function Marker(props) {
    return (
        <Tooltip title={props.text} placement="top">
            <Icon className="text-red">place</Icon>
        </Tooltip>
    );
}
*/



function Musteri(props) {
    const dispatch  = useDispatch();
    const musteri   = useSelector(({ musteriApp }) => musteriApp.musteri);
    const sektorler = useSelector(({ musteriApp }) => musteriApp.sektor.data);
    const theme     = useTheme();

    const [seciliIl, setSeciliIl] = useState(null);
    const [ilceler, setIlceler] = useState(null);
    const [tabValue, setTabValue] = useState(0);
    const { form, handleChange, setForm } = useForm(null);
    const inputLabel = React.useRef(null);

    useEffect(() => {
        function updateMusteriState() {
            const params = props.match.params;
            const { musteriId } = params;

            if (musteriId === 'new') {
                dispatch(Actions.newMusteri());
            }
            else {
                dispatch(Actions.getMusteri(props.match.params));
            }
            dispatch(Actions.getSektorler())
        }
        updateMusteriState()
    }, [dispatch, props.match.params]);

    useEffect(() => {
        if (props.match.params.musteriId === 'new') {
        }
        if (
            (musteri.data && !form) ||
            (musteri.data && form && musteri.data._id !== form._id)
        ) {
            setForm(musteri.data);
            setSeciliIl(musteri.data.il)
        }
    }, [form, musteri.data, setForm]);

    useEffect(() => {
        var ilceler = iller.find(il => il.il === seciliIl)
        if (ilceler) {
            setIlceler(ilceler.ilceleri)
        }
    }, [seciliIl])

    function handleChangeTab(event, tabValue) {
        setTabValue(tabValue);
    }

    function onIlSelected(e) {        
        setSeciliIl(e.target.value)
        handleChange(e)
    }

    function canBeSubmitted() {
        return (
            form.FirmaMarkasi.length > 0 &&
            form.FirmaUnvani.length > 0
            //!_.isEqual(product.data, form)
        );
    }

    if ((!musteri.data || (musteri.data && props.match.params.musteriId !== musteri.data._id)) && props.match.params.musteriId !== 'new') {
        return <FuseLoading />;
    }
    if (!sektorler) {
        return <FuseLoading />;
    }
    if (props.match.params.musteriId === 'new' && !form && !handleChange) {
        return <FuseLoading />;
    }
    if (seciliIl && !ilceler) {
        return <FuseLoading />;
    }

    return (
        <FusePageCarded
            classes={{
                toolbar: "p-0",
                header: "min-h-72 h-72 sm:h-136 sm:min-h-136"
            }}
            header={
                form && (
                    <div className="flex flex-1 w-full items-center justify-between">

                        <div className="flex flex-col items-start max-w-full">

                            <FuseAnimate animation="transition.slideRightIn" delay={300}>
                                <Typography className="normal-case flex items-center sm:mb-12" component={Link} role="button" to="/musteriler" color="inherit">
                                    <Icon className="text-20">{theme.direction === "ltr" ? "arrow_back" : "arrow_forward"}</Icon>
                                    <span className="mx-4">Müşteriler</span>
                                </Typography>
                            </FuseAnimate>

                            <div className="flex items-center max-w-full">
                                {/*
                                <FuseAnimate animation="transition.expandIn" delay={300}>
                                    {form.images.length > 0 && form.featuredImageId ? (
                                        <img className="w-32 sm:w-48 rounded" src={_.find(form.images, { id: form.featuredImageId }).url} alt={form.name} />
                                    ) : (
                                            <img className="w-32 sm:w-48 rounded" src="assets/images/ecommerce/product-image-placeholder.png" alt={form.name} />
                                    )}
                                </FuseAnimate>
                                */}
                                <div className="flex flex-col min-w-0 mx-8 sm:mc-16">
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography className="text-16 sm:text-20 truncate">
                                            {form.FirmaMarkasi ? form.FirmaMarkasi : 'Yeni Müşteri'}
                                        </Typography>
                                    </FuseAnimate>
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography variant="caption">Müşteri Detay</Typography>
                                    </FuseAnimate>
                                </div>
                            </div>
                        </div>
                        <FuseAnimate animation="transition.slideRightIn" delay={300}>
                            <Button
                                className="whitespace-no-wrap normal-case"
                                variant="contained"
                                color="secondary"
                                disabled={!canBeSubmitted()}
                                onClick={() => 
                                    {
                                        props.match.params.musteriId === 'new' ?  dispatch(Actions.createMusteri(form)) : dispatch(Actions.saveMusteri(form))
                                    }
                                }
                            >
                                Kaydet
                            </Button>
                        </FuseAnimate>
                    </div>
                )
            }
            contentToolbar={
                <Tabs
                    value={tabValue}
                    onChange={handleChangeTab}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="scrollable"
                    scrollButtons="auto"
                    classes={{ root: "w-full h-64" }}
                >
                    <Tab className="h-64 normal-case" label="Müşteri bilgileri" />
                    <Tab className="h-64 normal-case" label="Adres ve İletişim" />
                    <Tab className="h-64 normal-case" label="Kişiler" />
                    <Tab className="h-64 normal-case" label="Mali Bilgiler" />
                    <Tab className="h-64 normal-case" label="." />
                </Tabs>
            }
            content={
                form && (
                    <div className="p-16 sm:p-24 max-w-2xl">
                        {tabValue === 0 &&
                            (
                                <div>

                                    <TextField
                                        className="mt-8 mb-16"
                                        error={form.FirmaMarkasi === ''}
                                        required
                                        label="Marka"
                                        autoFocus
                                        id="FirmaMarkasi"
                                        name="FirmaMarkasi"
                                        value={form.FirmaMarkasi}
                                        onChange={handleChange}
                                        variant="outlined"
                                        fullWidth
                                    />

                                    <TextField
                                        required
                                        className="mt-8 mb-16"
                                        id="FirmaUnvani"
                                        name="FirmaUnvani"
                                        onChange={handleChange}
                                        label="Firma Unvani"
                                        type="text"
                                        value={form.FirmaUnvani}
                                        variant="outlined"
                                        fullWidth
                                    />

                                    <InputLabel ref={inputLabel} id="sektor-select-outlined-label">
                                        Sektor
                                    </InputLabel>
                                    <Select
                                        fullWidth
                                        labelId="sektor-select-outlined-label"
                                        className="mt-8 mb-16"
                                        name="Sektor"
                                        value={form.Sektor}
                                        onChange={handleChange}
                                        input={
                                            <OutlinedInput
                                                labelWidth={("category".length * 9)}
                                                name="category"
                                                id="category-label-placeholder"
                                            />
                                        }
                                    >
                                        {sektorler.map(sektor => (
                                            <MenuItem value={sektor._id} key={sektor._id}>{sektor.Adi}</MenuItem>
                                        ))}
                                    </Select>


                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                name="KobiMi"
                                                checked={form.KobiMi}
                                                onChange={handleChange}
                                                value={form.KobiMi}
                                                color="primary"
                                            />
                                        }
                                        label="Kobi mi?"
                                    />

                                </div>
                            )}
                        {tabValue === 1 && (
                            <div>
                                <InputLabel ref={inputLabel} id="il-select-outlined-label">
                                    İl
                                    </InputLabel>
                                <Select
                                    fullWidth
                                    labelId="il-select-outlined-label"
                                    className="mt-8 mb-16"
                                    name="il"
                                    value={form.il}
                                    onChange={onIlSelected}
                                    input={
                                        <OutlinedInput
                                            labelWidth={("category".length * 9)}
                                            name="category"
                                            id="category-label-placeholder"
                                        />
                                    }
                                >
                                    {iller.map(il => (
                                        <MenuItem value={il.il} key={il.il}>{il.il}</MenuItem>
                                    ))}
                                </Select>

                                <InputLabel ref={inputLabel} id="ilce-select-outlined-label">
                                    İlçe
                                    </InputLabel>
                                <Select
                                    fullWidth
                                    labelId="ilce-select-outlined-label"
                                    className="mt-8 mb-16"
                                    name="ilce"
                                    value={form.ilce}
                                    onChange={handleChange}
                                    input={
                                        <OutlinedInput
                                            labelWidth={("category".length * 9)}
                                            name="category"
                                            id="category-label-placeholder"
                                        />
                                    }
                                >
                                    {ilceler && ilceler.map(ilce => (
                                        <MenuItem value={ilce} key={ilce}>{ilce}</MenuItem>
                                    ))}
                                </Select>

                                <TextField
                                    className="mt-8 mb-16"
                                    label="Adres"
                                    id="Adres"
                                    name="Adres"
                                    value={form.Adres}
                                    onChange={handleChange}
                                    type="text"
                                    variant="outlined"
                                    rows={3}
                                    multiline
                                    fullWidth
                                />
                                <TextField
                                    className="mt-8 mb-16"
                                    label="OSB"
                                    id="OSB"
                                    name="OSB"
                                    value={form.OSB}
                                    onChange={handleChange}
                                    type="text"
                                    variant="outlined"
                                    fullWidth
                                />
                                <TextField
                                    className="mt-8 mb-16"
                                    label="Web"
                                    id="Web"
                                    name="Web"
                                    value={form.Web}
                                    onChange={handleChange}
                                    type="text"
                                    variant="outlined"
                                    fullWidth
                                />
                                <TextField
                                    className="mt-8 mb-16"
                                    label="Telefon"
                                    id="Telefon"
                                    name="Telefon"
                                    value={form.Telefon}
                                    onChange={handleChange}
                                    type="text"
                                    variant="outlined"
                                    fullWidth
                                />
                                <TextField
                                    className="mt-8 mb-16"
                                    label="Mail"
                                    id="Mail"
                                    name="Mail"
                                    value={form.Mail}
                                    onChange={handleChange}
                                    type="text"
                                    variant="outlined"
                                    fullWidth
                                />

                                <Card className="w-full h-512 rounded-8 shadow-none border-1">

                                    <GoogleMap
                                        bootstrapURLKeys={{
                                            key: process.env.REACT_APP_MAP_KEY
                                        }}
                                        defaultZoom={1}
                                        defaultCenter={[17.308688, 7.03125]}
                                    //options={{
                                    //    styles: props.data.styles
                                    //}}
                                    >
                                        {/*props.data.markers.map(marker => (
                                        <Marker
                                            key={marker.label}
                                            text={marker.label}
                                            lat={marker.lat}
                                            lng={marker.lng}
                                        />
                                    ))*/}
                                    </GoogleMap>
                                </Card>
                            </div>
                        )}
                        {tabValue === 2 && (
                            <div>


                            </div>
                        )}
                        {tabValue === 3 && (
                            <div>

                                <TextField
                                    className="mt-8 mb-16"
                                    label="Vergi Dairesi"
                                    autoFocus
                                    id="vergiDairesi"
                                    name="vergiDairesi"
                                    value={form.vergiDairesi}
                                    onChange={handleChange}
                                    variant="outlined"
                                    fullWidth
                                />

                                <TextField
                                    className="mt-8 mb-16"
                                    label="Vergi No"
                                    id="vergiNo"
                                    name="vergiNo"
                                    value={form.vergiNo}
                                    onChange={handleChange}
                                    variant="outlined"
                                    type="text"
                                    fullWidth
                                />
                                <TextField
                                    className="mt-8 mb-16"
                                    label="Ciro"
                                    id="ciro"
                                    name="ciro"
                                    value={form.ciro}
                                    onChange={handleChange}
                                    variant="outlined"
                                    type="number"
                                    fullWidth
                                />
                                <TextField
                                    className="mt-8 mb-16"
                                    label="Calisan Sayisi"
                                    id="calisanSayisi"
                                    name="calisanSayisi"
                                    value={form.calisanSayisi}
                                    onChange={handleChange}
                                    variant="outlined"
                                    type="number"
                                    fullWidth
                                />
                                <TextField
                                    className="mt-8 mb-16"
                                    label="Teknik Personel Sayisi"
                                    id="teknikPersonelSayisi"
                                    name="teknikPersonelSayisi"
                                    value={form.teknikPersonelSayisi}
                                    onChange={handleChange}
                                    variant="outlined"
                                    type="number"
                                    fullWidth
                                />
                                <TextField
                                    className="mt-8 mb-16"
                                    label="Muhendis Sayisi"
                                    id="muhendisSayisi"
                                    name="muhendisSayisi"
                                    value={form.muhendisSayisi}
                                    onChange={handleChange}
                                    variant="outlined"
                                    type="number"
                                    fullWidth
                                />
                            </div>
                        )}
                        {tabValue === 4 && (
                            <div>
                                <div className="flex -mx-4">
                                    <TextField
                                        className="mt-8 mb-16 mx-4"
                                        label="Width"
                                        autoFocus
                                        id="width"
                                        name="width"
                                        value={form.width}
                                        onChange={handleChange}
                                        variant="outlined"
                                        fullWidth
                                    />

                                    <TextField
                                        className="mt-8 mb-16 mx-4"
                                        label="Height"
                                        id="height"
                                        name="height"
                                        value={form.height}
                                        onChange={handleChange}
                                        variant="outlined"
                                        fullWidth
                                    />

                                    <TextField
                                        className="mt-8 mb-16 mx-4"
                                        label="Depth"
                                        id="depth"
                                        name="depth"
                                        value={form.depth}
                                        onChange={handleChange}
                                        variant="outlined"
                                        fullWidth
                                    />

                                </div>

                                <TextField
                                    className="mt-8 mb-16"
                                    label="Weight"
                                    id="weight"
                                    name="weight"
                                    value={form.weight}
                                    onChange={handleChange}
                                    variant="outlined"
                                    fullWidth
                                />

                                <TextField
                                    className="mt-8 mb-16"
                                    label="Extra Shipping Fee"
                                    id="extraShippingFee"
                                    name="extraShippingFee"
                                    value={form.extraShippingFee}
                                    onChange={handleChange}
                                    variant="outlined"
                                    InputProps={{
                                        startAdornment: <InputAdornment position="start">$</InputAdornment>
                                    }}
                                    fullWidth
                                />
                            </div>
                        )}
                    </div>
                )
            }
            innerScroll
        />
    )
}

export default withReducer('musteriApp', reducer)(Musteri);
