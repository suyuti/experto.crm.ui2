import React, {useEffect, useState} from 'react';
import {Checkbox, Icon, IconButton, Typography} from '@material-ui/core';
import {FuseUtils, FuseAnimate} from '@fuse';
import {useDispatch, useSelector} from 'react-redux';
import ReactTable from "react-table";
import * as Actions from '../store/actions';
import MusterilerMultiSelectMenu from './MusterilerMultiSelectMenu';
import {withRouter} from 'react-router-dom';

function MusterilerList(props)
{
    const dispatch = useDispatch();
    const contacts = useSelector(({musteriApp}) =>  musteriApp.musteriler.entities);
    const selectedMusteriIds = useSelector(({musteriApp}) => musteriApp.musteriler.selectedMusteriIds);
    const searchText = useSelector(({musteriApp}) => musteriApp.musteriler.searchText);
    const user = useSelector(({musteriApp}) => musteriApp.user);

    const [filteredData, setFilteredData] = useState(null);

    useEffect(() => {
        function getFilteredArray(entities, searchText)
        {
            const arr = Object.keys(entities).map((id) => entities[id]);
            if ( searchText.length === 0 )
            {
                return arr;
            }
            return FuseUtils.filterArrayByString(arr, searchText);
        }

        if ( contacts )
        {
            setFilteredData(getFilteredArray(contacts, searchText));
        }
    }, [contacts, searchText]);


    if ( !filteredData )
    {
        return null;
    }

    if ( filteredData.length === 0 )
    {
        return (
            <div className="flex flex-1 items-center justify-center h-full">
                <Typography color="textSecondary" variant="h5">
                    There are no contacts!
                </Typography>
            </div>
        );
    }

    return (
        <FuseAnimate animation="transition.slideUpIn" delay={300}>
            <ReactTable
                className="-striped -highlight h-full sm:rounded-16 overflow-hidden"
                getTrProps={(state, rowInfo, column) => {
                    return {
                        className: "cursor-pointer",
                        onClick  : (e, handleOriginal) => {
                            if ( rowInfo )
                            {
                                //dispatch(Actions.openEditContactDialog(rowInfo.original));
                                props.history.push('/musteriApp/musteri/' + rowInfo.original._id);
                            }
                        }
                    }
                }}
                data={filteredData}
                columns={[
                    {
                        Header   : () => (
                            <Checkbox
                                onClick={(event) => {
                                    event.stopPropagation();
                                }}
                                onChange={(event) => {
                                    event.target.checked ? dispatch(Actions.selectAllMusteriler()) : dispatch(Actions.deselectAllMusteriler());
                                }}
                                checked={selectedMusteriIds.length === Object.keys(contacts).length && selectedMusteriIds.length > 0}
                                indeterminate={selectedMusteriIds.length !== Object.keys(contacts).length && selectedMusteriIds.length > 0}
                            />
                        ),
                        accessor : "",
                        Cell     : row => {
                            return (<Checkbox
                                    onClick={(event) => {
                                        event.stopPropagation();
                                    }}
                                    checked={selectedMusteriIds.includes(row.value._id)}
                                    onChange={() => dispatch(Actions.toggleInSelectedMusteri(row.value._id))}
                                />
                            )
                        },
                        className: "justify-center",
                        sortable : false,
                        width    : 64
                    },
                    /*{
                        Header   : () => (
                            //selectedMusteriIds.length > 0 && (
                                <MusteriMultiSelectMenu/>
                            //)
                        ),
                        accessor : "avatar",
                        Cell     : row => (
                            <Avatar className="mx-8" alt={row.original.name} src={row.value}/>
                        ),
                        className: "justify-center",
                        width    : 64,
                        sortable : false
                    },*/
                    {
                        Header    : "Firma Marka",
                        accessor  : "FirmaMarkasi",
                        filterable: true,
                        className : "font-bold"
                    },
                    {
                        Header    : "Firma Unvani",
                        accessor  : "FirmaUnvani",
                        filterable: true,
                        className : "font-bold"
                    },/*
                    {
                        Header    : "Sektor",
                        accessor  : , //d => d.Sektor ? d.Sektor.Adi : '',// d.Sektor.Adi,
                        filterable: true
                    },*/
                    {
                        Header    : "İl",
                        accessor  : "il",
                        filterable: true
                    },
                    {
                        Header    : "İlçe",
                        accessor  : "ilce",
                        filterable: true
                    },
                    {
                        Header    : "Phone",
                        accessor  : "phone",
                        filterable: true
                    },
                    {
                        Header: "",
                        width : 128,
                        Cell  : row => (
                            <div className="flex items-center">
                                <IconButton
                                    onClick={(ev) => {
                                        ev.stopPropagation();
                                        dispatch(Actions.toggleStarredContact(row.original.id))
                                    }}
                                >
                                    {user.starred && user.starred.includes(row.original.id) ? (
                                        <Icon>star</Icon>
                                    ) : (
                                        <Icon>star_border</Icon>
                                    )}
                                </IconButton>
                                <IconButton
                                    onClick={(ev) => {
                                        ev.stopPropagation();
                                        dispatch(Actions.removeMusteri(row.original._id));
                                    }}
                                >
                                    <Icon>delete</Icon>
                                </IconButton>
                            </div>
                        )
                    }
                ]}
                defaultPageSize={10}
                noDataText="No contacts found"
            />
        </FuseAnimate>
    );
}

export default withRouter(MusterilerList);
