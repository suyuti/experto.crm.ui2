import React, { useEffect, useState } from 'react';
import {
    Button,
    Tab,
    Tabs,
    TextField,
    InputAdornment,
    Icon,
    Typography,
} from '@material-ui/core';
import { useTheme } from '@material-ui/styles';
import { FuseAnimate, FusePageCarded, FuseLoading } from '@fuse';
import { useForm } from '@fuse/hooks';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import withReducer from 'app/store/withReducer';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';

function Urun(props) {
    const dispatch  = useDispatch();
    const urun      = useSelector(({ urunApp }) => urunApp.urun);
    const theme     = useTheme();

    const [tabValue, setTabValue] = useState(0);
    const { form, handleChange, setForm } = useForm(null);

    useEffect(() => {
        function updateUrunState() {
            const params = props.match.params;
            const { urunId } = params;
            if (urunId === 'new') {
                dispatch(Actions.newUrun());
            }
            else {
                dispatch(Actions.getUrun(props.match.params));
            }
        } 
        updateUrunState()
    }, [dispatch, props.match.params]);

    useEffect(() => {
        if (
            (urun.data && !form) ||
            (urun.data && form && urun.data._id !== form.id)
        ) {
            setForm(urun.data);
        }
    }, [form, urun.data, setForm]);
    

    function handleChangeTab(event, tabValue) {
        setTabValue(tabValue);
    }


    function canBeSubmitted() {
        return (
            form.Adi.length > 0 
            //&&
            //form.FirmaUnvani.length > 0
            //!_.isEqual(product.data, form)
        );
    }

    
    if ((!urun.data || (urun.data && props.match.params.urunId !== urun.data._id)) && props.match.params.urunId !== 'new') {
        return <FuseLoading />;
    }
    
//    if (props.match.params.urunId !== urun.data._id) {
//        return <FuseLoading />;
//    }

    if (props.match.params.urunId === 'new' && !form && !handleChange) {
        return <FuseLoading />;
    }
    //if (props.match.params.urunId == 'new' && !form) {
    //    return <FuseLoading />;
    //}

    //if (IsLoading == true || IsSektorLoading == true) {
    //    return <FuseLoading />;
    //}

    function TutarInputHandler(e) {
        let val = e.target.value.length == 0 ? 0 : e.target.value
        let tutar = parseInt(val)
        if (!isNaN(val)) {
            handleChange(e)
        }
    }

    console.log(form)


    return (
        <FusePageCarded
            classes={{
                toolbar: "p-0",
                header: "min-h-72 h-72 sm:h-136 sm:min-h-136"
            }}
            header={
                form && (
                    <div className="flex flex-1 w-full items-center justify-between">

                        <div className="flex flex-col items-start max-w-full">

                            <FuseAnimate animation="transition.slideRightIn" delay={300}>
                                <Typography className="normal-case flex items-center sm:mb-12" component={Link} role="button" to="/urunler" color="inherit">
                                    <Icon className="text-20">{theme.direction === "ltr" ? "arrow_back" : "arrow_forward"}</Icon>
                                    <span className="mx-4">Ürünler</span>
                                </Typography>
                            </FuseAnimate>

                            <div className="flex items-center max-w-full">
                                {/*
                                <FuseAnimate animation="transition.expandIn" delay={300}>
                                    {form.images.length > 0 && form.featuredImageId ? (
                                        <img className="w-32 sm:w-48 rounded" src={_.find(form.images, { id: form.featuredImageId }).url} alt={form.name} />
                                    ) : (
                                            <img className="w-32 sm:w-48 rounded" src="assets/images/ecommerce/product-image-placeholder.png" alt={form.name} />
                                    )}
                                </FuseAnimate>
                                */}
                                <div className="flex flex-col min-w-0 mx-8 sm:mc-16">
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography className="text-16 sm:text-20 truncate">
                                            {form.FirmaMarkasi ? form.FirmaMarkasi : 'Yeni Ürün'}
                                        </Typography>
                                    </FuseAnimate>
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography variant="caption">Ürün Detay</Typography>
                                    </FuseAnimate>
                                </div>
                            </div>
                        </div>
                        <FuseAnimate animation="transition.slideRightIn" delay={300}>
                            <Button
                                className="whitespace-no-wrap normal-case"
                                variant="contained"
                                color="secondary"
                                disabled={!canBeSubmitted()}
                                onClick={() => dispatch(Actions.saveUrun(form))}
                            >
                                Kaydet
                            </Button>
                        </FuseAnimate>
                    </div>
                )
            }
            contentToolbar={
                <Tabs
                    value={tabValue}
                    onChange={handleChangeTab}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="scrollable"
                    scrollButtons="auto"
                    classes={{ root: "w-full h-64" }}
                >
                    <Tab className="h-64 normal-case" label="Ürün bilgileri" />
                    <Tab className="h-64 normal-case" label="Fiyat bilgileri" />
                    <Tab className="h-64 normal-case" label="Diğer" />
                </Tabs>
            }
            content={
                form && (
                    <div className="p-16 sm:p-24 max-w-2xl">
                        {tabValue === 0 &&
                            (
                                <div>

                                    <TextField
                                        className="mt-8 mb-16"
                                        error={form.Adi === ''}
                                        required
                                        label="Ürün Adı"
                                        autoFocus
                                        id="Adi"
                                        name="Adi"
                                        value={form.Adi}
                                        onChange={handleChange}
                                        variant="outlined"
                                        fullWidth
                                    />
                                </div>
                            )}
                        {tabValue === 1 && (
                            <div>
                            <TextField
                                className="mt-8 mb-16"
                                label="Ön Ödeme Tutarı"
                                id="OnOdemeTutari"
                                name="OnOdemeTutari"
                                value={form.OnOdemeTutari}
                                onChange={TutarInputHandler}
                                type="text"
                                variant="outlined"
                                autoFocus
                                fullWidth
                                InputProps={{
                                    startAdornment: <InputAdornment position="start">TL</InputAdornment>
                                }}
                                    />
                            <TextField
                                className="mt-8 mb-16"
                                label="Sabit Tutar"
                                id="SabitTutar"
                                name="SabitTutar"
                                value={form.SabitTutar}
                                onChange={TutarInputHandler}
                                type="text"
                                variant="outlined"
                                fullWidth
                                InputProps={{
                                    startAdornment: <InputAdornment position="start">TL</InputAdornment>
                                }}
                                    />
                            <TextField
                                className="mt-8 mb-16"
                                label="Rapor Başına Ödeme Tutarı"
                                id="RaporBasiOdemeTutari"
                                name="RaporBasiOdemeTutari"
                                value={form.RaporBasiOdemeTutari}
                                onChange={TutarInputHandler}
                                type="text"
                                variant="outlined"
                                fullWidth
                                InputProps={{
                                    startAdornment: <InputAdornment position="start">TL</InputAdornment>
                                }}
                                    />
                            <TextField
                                className="mt-8 mb-16"
                                label="Yüzde Tutarı"
                                id="YuzdeTutari"
                                name="YuzdeTutari"
                                value={form.YuzdeTutari}
                                onChange={TutarInputHandler}
                                type="text"
                                variant="outlined"
                                fullWidth
                                InputProps={{
                                    startAdornment: <InputAdornment position="start">TL</InputAdornment>
                                }}
                                    />
                            <TextField
                                className="mt-8 mb-16"
                                label="Başarı Tutarı"
                                id="BasariTutari"
                                name="BasariTutari"
                                value={form.BasariTutari}
                                onChange={TutarInputHandler}
                                type="text"
                                variant="outlined"
                                fullWidth
                                InputProps={{
                                    startAdornment: <InputAdornment position="start">TL</InputAdornment>
                                }}
                                    />
                            <TextField
                                className="mt-8 mb-16"
                                label="KDV Oranı"
                                id="KDVOrani"
                                name="KDVOrani"
                                value={form.KDVOrani}
                                onChange={TutarInputHandler}
                                type="text"
                                variant="outlined"
                                fullWidth
                                InputProps={{
                                    startAdornment: <InputAdornment position="start">TL</InputAdornment>
                                }}
                                    />
                            </div>
                        )}
                        {tabValue === 2 && (
                            <div>


                            </div>
                        )}
                    </div>
                )
            }
            innerScroll
        />
    )
}

export default withReducer('urunApp', reducer)(Urun);
