import axios from 'axios';

export const GET_URUNLER             = '[URUN] GET URUNLER';
export const SELECT_ALL_URUNLER      = '[URUN] SELECT ALL URUNLER';
export const DESELECT_ALL_URUNLER    = '[URUN] DESELECT ALL URUNLER';
export const OPEN_NEW_URUN_DIALOG    = '[URUN] OPEN NEW URUN DIALOG';
export const CLOSE_NEW_URUN_DIALOG   = '[URUN] CLOSE NEW URUN DIALOG';
export const SET_SEARCH_TEXT            = '[URUN APP] SET SEARCH TEXT';
export const TOGGLE_IN_SELECTED_URUN = '[URUN APP] TOGGLE IN SELECTED URUN';

export function getUrunler(routeParams)
{
    const request = axios.get('/api/urun', {
        params: routeParams
    });

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_URUNLER,
                payload: response.data,
                routeParams
            })
        );
}

export function selectAllUrunler()
{
    return {
        type: SELECT_ALL_URUNLER
    }
}

export function deselectAllUrunler()
{
    return {
        type: DESELECT_ALL_URUNLER
    }
}

export function setSearchText(event)
{
    return {
        type      : SET_SEARCH_TEXT,
        searchText: event.target.value
    }
}

export function toggleInSelectedUrun(urunId)
{
    return {
        type: TOGGLE_IN_SELECTED_URUN,
        urunId
    }
}


/*
export function openNewMusteriDialog()
{
    return {
        type: OPEN_NEW_MUSTERI_DIALOG
    }
}

export function closeNewMusteriDialog()
{
    return {
        type: CLOSE_NEW_MUSTERI_DIALOG
    }
}
*/