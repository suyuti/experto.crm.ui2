import axios from 'axios';
import {showMessage} from 'app/store/actions/fuse';


export const GET_URUN             = '[URUN APP] GET URUNLER';
export const SAVE_URUN            = '[URUN APP] SAVE URUNLER';


export function getUrun(params)
{
    const request = axios.get('/api/urun', {params});

    return (dispatch) =>
        request.then((response) => {
            dispatch({
                type   : GET_URUN,
                payload: response.data
            })}
        );
}

export function saveUrun(data)
{
    const request = axios.post('/api/urun', data);

    return (dispatch) =>
        request.then((response) => {

                dispatch(showMessage({message: 'Urun kaydedildi.'}));

                return dispatch({
                    type   : SAVE_URUN,
                    payload: response.data
                })
            }
        );
}

export function newUrun()
{
    const data = {
        id              : '',//FuseUtils.generateGUID(),
        Adi: '',
        OnOdemeTutari: '',
        SabitTutar: '',
        RaporBasiOdemeTutari: '',
        YuzdeTutari: '', 
        BasariTutari: '',
        KDVOrani: '',
    };

    return {
        type   : GET_URUN,
        payload: {data}
    }
}

