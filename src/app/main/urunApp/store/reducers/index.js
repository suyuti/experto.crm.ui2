import {combineReducers} from 'redux';
import urun from './urun.reducer';
import urunler from './urunler.reducer';

const reducer = combineReducers({
    urun,
    urunler
});

export default reducer;
