import * as Actions from '../actions';
import _ from '@lodash';

const initialState = {
    entities: null,
    searchText: '',
    selectedUrunIds: [],
    routeParams: {},
    urunDialog: {
        type: 'new',
        props: {
            open: false
        },
        data: null
    }
};

const urunlerReducer = function (state = initialState, action) {
    switch (action.type) {
        case Actions.GET_URUNLER:
            {
                return {
                    ...state,
                    entities: _.keyBy(action.payload.data, '_id'),
                    routeParams: action.routeParams
                };
            }
        case Actions.SELECT_ALL_URUNLER:
            {
                const arr = Object.keys(state.entities).map(k => state.entities[k]);

                const selectedUrunIds = arr.map(urun => urun._id);

                return {
                    ...state,
                    selectedUrunIds: selectedUrunIds
                };
            }
        case Actions.DESELECT_ALL_URUNLER:
            {
                return {
                    ...state,
                    selectedUrunIds: []
                };
            }
        case Actions.OPEN_NEW_URUN_DIALOG:
            {
                return {
                    ...state,
                    urunDialog: {
                        type: 'new',
                        props: {
                            open: true
                        },
                        data: null
                    }
                };
            }
        case Actions.CLOSE_NEW_URUN_DIALOG:
            {
                return {
                    ...state,
                    urunDialog: {
                        type: 'new',
                        props: {
                            open: false
                        },
                        data: null
                    }
                };
            }
        case Actions.SET_SEARCH_TEXT:
            {
                return {
                    ...state,
                    searchText: action.searchText
                };
            }
        case Actions.TOGGLE_IN_SELECTED_URUN:
            {

                const urunId = action.urunId;

                let selectedUrunIds = [...state.selectedUrunIds];

                if (selectedUrunIds.find(id => id === urunId) !== undefined) {
                    selectedUrunIds = selectedUrunIds.filter(id => id !== urunId);
                }
                else {
                    selectedUrunIds = [...selectedUrunIds, urunId];
                }

                return {
                    ...state,
                    selectedUrunIds: selectedUrunIds
                };
            }


        default:
            {
                return state;
            }
    }
};


export default urunlerReducer;
