import React, {useEffect, useState} from 'react';
import {Checkbox, Icon, IconButton, Typography} from '@material-ui/core';
import {FuseUtils, FuseAnimate} from '@fuse';
import {useDispatch, useSelector} from 'react-redux';
import ReactTable from "react-table";
import * as Actions from '../store/actions';
import {withRouter} from 'react-router-dom';

function UrunlerList(props)
{
    const dispatch          = useDispatch();
    const urunler           = useSelector(({urunApp}) => urunApp.urunler.entities);
    const selectedUrunIds   = useSelector(({urunApp}) => urunApp.urunler.selectedUrunIds);
    const searchText        = useSelector(({urunApp}) => urunApp.urunler.searchText);
    //const user = useSelector(({musteriApp}) => musteriApp.user);

    const [filteredData, setFilteredData] = useState(null);

    useEffect(() => {
        function getFilteredArray(entities, searchText)
        {
            const arr = Object.keys(entities).map((_id) => entities[_id]);
            if ( searchText.length === 0 )
            {
                return arr;
            }
            return FuseUtils.filterArrayByString(arr, searchText);
        }

        if ( urunler )
        {
            setFilteredData(getFilteredArray(urunler, searchText));
        }
    }, [urunler, searchText]);


    if ( !filteredData )
    {
        return null;
    }

    if ( filteredData.length === 0 )
    {
        return (
            <div className="flex flex-1 items-center justify-center h-full">
                <Typography color="textSecondary" variant="h5">
                    Ürün yok!
                </Typography>
            </div>
        );
    }

    return (
        <FuseAnimate animation="transition.slideUpIn" delay={300}>
            <ReactTable
                className="-striped -highlight h-full sm:rounded-16 overflow-hidden"
                getTrProps={(state, rowInfo, column) => {
                    return {
                        className: "cursor-pointer",
                        onClick  : (e, handleOriginal) => {
                            if ( rowInfo )
                            {
                                //dispatch(Actions.openEditContactDialog(rowInfo.original));
                                props.history.push('/urun/' + rowInfo.original._id);
                            }
                        }
                    }
                }}
                data={filteredData}
                columns={[
                    {
                        Header   : () => (
                            <Checkbox
                                onClick={(event) => {
                                    event.stopPropagation();
                                }}
                                onChange={(event) => {
                                    event.target.checked ? dispatch(Actions.selectAllUrunler()) : dispatch(Actions.deselectAllUrunler());
                                }}
                                checked={selectedUrunIds.length === Object.keys(urunler).length && selectedUrunIds.length > 0}
                                indeterminate={selectedUrunIds.length !== Object.keys(urunler).length && selectedUrunIds.length > 0}
                            />
                        ),
                        accessor : "",
                        Cell     : row => {
                            return (<Checkbox
                                    onClick={(event) => {
                                        event.stopPropagation();
                                    }}
                                    checked={selectedUrunIds.includes(row.value._id)}
                                    onChange={() => dispatch(Actions.toggleInSelectedUrun(row.value._id))}
                                />
                            )
                        },
                        className: "justify-center",
                        sortable : false,
                        width    : 64
                    },
                    /*{
                        Header   : () => (
                            //selectedMusteriIds.length > 0 && (
                                <MusteriMultiSelectMenu/>
                            //)
                        ),
                        accessor : "avatar",
                        Cell     : row => (
                            <Avatar className="mx-8" alt={row.original.name} src={row.value}/>
                        ),
                        className: "justify-center",
                        width    : 64,
                        sortable : false
                    },*/
                    {
                        Header    : "Ürün Adı",
                        accessor  : "Adi",
                        filterable: true,
                        className : "font-bold"
                    },
                    {
                        Header: "",
                        width : 128,
                        Cell  : row => (
                            <div className="flex items-center">
                                <IconButton
                                    onClick={(ev) => {
                                        ev.stopPropagation();
                                        //dispatch(Actions.toggleStarredContact(row.original.id))
                                    }}
                                >
                                    {/*user.starred && user.starred.includes(row.original.id) ? (
                                        <Icon>star</Icon>
                                    ) : (
                                        <Icon>star_border</Icon>
                                    )*/}
                                </IconButton>
                                <IconButton
                                    onClick={(ev) => {
                                        ev.stopPropagation();
                                        //dispatch(Actions.removeContact(row.original.id));
                                    }}
                                >
                                    <Icon>delete</Icon>
                                </IconButton>
                            </div>
                        )
                    }
                ]}
                defaultPageSize={10}
                noDataText="No contacts found"
            />
        </FuseAnimate>
    );
}

export default withRouter(UrunlerList);
