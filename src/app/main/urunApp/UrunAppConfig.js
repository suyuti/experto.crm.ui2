import React from 'react';
import {authRoles} from 'app/auth';
import _ from '@lodash';

export const UrunAppConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    auth    : _.union(authRoles.superadmin,
                      authRoles.admin,
                      authRoles.yonetici, 
                      authRoles.satis_yonetici, 
                      authRoles.satis_personel),
    routes  : [
        {
            path     : '/urunler',
            component: React.lazy(() => import('./urunler/Urunler'))
        },
        {
            path     : '/urun/:urunId/:urunHandle?',
            component: React.lazy(() => import('./urun/Urun'))
        }
    ]
};

