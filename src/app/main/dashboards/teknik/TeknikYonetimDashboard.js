import React, { useRef, useState} from 'react';
import {Menu,  Hidden, Icon, IconButton, Tab, Tabs, Typography} from '@material-ui/core';
import {FuseAnimateGroup, FusePageSimple} from '@fuse';
import withReducer from 'app/store/withReducer';
import reducer from '../satis/store/reducers';
import clsx from 'clsx';

/*
import Widget1 from './widgets/Widget1';
*/
import Widget2 from '../satis/widgets/Widget2';
/*import Widget3 from './widgets/Widget3';
import Widget4 from './widgets/Widget4';
import Widget5 from './widgets/Widget5';
import Widget6 from './widgets/Widget6';
import Widget7 from './widgets/Widget7';
import Widget8 from './widgets/Widget8';
import Widget9 from './widgets/Widget9';
import Widget10 from './widgets/Widget10';
import Widget11 from './widgets/Widget11';
*/import WidgetNow from '../satis/widgets/WidgetNow';
import WidgetTeklifler from '../satis/widgets/WidgetTeklifler'

import {makeStyles} from '@material-ui/styles';

const useStyles = makeStyles(theme => ({
    content          : {
        '& canvas': {
            maxHeight: '100%'
        }
    },
    selectedProject  : {
        background  : theme.palette.primary.main,
        color       : theme.palette.primary.contrastText,
        borderRadius: '8px 0 0 0'
    },
    projectMenuButton: {
        background  : theme.palette.primary.main,
        color       : theme.palette.primary.contrastText,
        borderRadius: '0 8px 0 0',
        marginLeft  : 1
    },
}));

function TeknikYonetimDashboard(props)
{
    //const dispatch = useDispatch();
    //const widget2 = useSelector(({satisDashboard}) => satisDashboard.widget2);
    //const teklifler = useSelector(({satisDashboard}) => satisDashboard.teklifler);

    const classes = useStyles(props);
    const pageLayout = useRef(null);
    const [tabValue, setTabValue] = useState(0);
    const [selectedProject, setSelectedProject] = useState({
        id    : 1,
        menuEl: null
    });


    //useEffect(async () => {
        //dispatch(Actions.getWidget2());
        //dispatch(Actions.getTeklifler());
    //}, [dispatch]);


    function handleChangeTab(event, tabValue)
    {
        setTabValue(tabValue);
    }


    function handleOpenProjectMenu(event)
    {
        setSelectedProject({
            id    : selectedProject.id,
            menuEl: event.currentTarget
        });
    }

    function handleCloseProjectMenu()
    {
        setSelectedProject({
            id    : selectedProject.id,
            menuEl: null
        });
    }
    return (
        <FusePageSimple
            classes={{
                header      : "min-h-160 h-160",
                toolbar     : "min-h-48 h-48",
                rightSidebar: "w-288",
                content     : classes.content,
            }}
            header={
                <div className="flex flex-col justify-between flex-1 px-24 pt-24">
                    <div className="flex justify-between items-start">
                        <Typography className="py-0 sm:py-24" variant="h4">Teknik Yönetim masası</Typography>
                        <Hidden lgUp>
                            <IconButton
                                onClick={(ev) => pageLayout.current.toggleRightSidebar()}
                                aria-label="open left sidebar"
                            >
                                <Icon>menu</Icon>
                            </IconButton>
                        </Hidden>
                    </div>
                    <div className="flex items-end">
                        <div className="flex items-center">
                            <div className={clsx(classes.selectedProject, "flex items-center h-40 px-16 text-16")}>
                                {/*_.find(teklifler, ['id', selectedProject.id]).name*/}
                            </div>
                            <IconButton
                                className={clsx(classes.projectMenuButton, "h-40 w-40 p-0")}
                                aria-owns={selectedProject.menuEl ? 'project-menu' : undefined}
                                aria-haspopup="true"
                                onClick={handleOpenProjectMenu}
                            >
                                <Icon>more_horiz</Icon>
                            </IconButton>
                            <Menu
                                id="project-menu"
                                anchorEl={selectedProject.menuEl}
                                open={Boolean(selectedProject.menuEl)}
                                onClose={handleCloseProjectMenu}
                            >
                                {/*teklifler && teklifler.map(project => (
                                    <MenuItem key={project.id} onClick={ev => {
                                        handleChangeProject(project.id)
                                    }}>{project.name}</MenuItem>
                                ))*/}
                            </Menu>
                        </div>
                    </div>
                </div>
            }
            contentToolbar={
                <Tabs
                    value={tabValue}
                    onChange={handleChangeTab}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="scrollable"
                    scrollButtons="off"
                    className="w-full border-b-1 px-24"
                >
                    <Tab className="text-14 font-600 normal-case" label="Ana ekran"/>
                    <Tab className="text-14 font-600 normal-case" label="İşlerim"/>
                    <Tab className="text-14 font-600 normal-case" label="."/>
                </Tabs>
            }
            content={
                <div className="p-12">
                    {tabValue === 0 &&
                    (
                        <FuseAnimateGroup
                            className="flex flex-wrap"
                            enter={{
                                animation: "transition.slideUpBigIn"
                            }}
                        >
                        </FuseAnimateGroup>
                    )}
                    {tabValue === 1 && (
                        <FuseAnimateGroup
                            className="flex flex-wrap"
                            enter={{
                                animation: "transition.slideUpBigIn"
                            }}
                        >
                        </FuseAnimateGroup>
                    )}
                    {tabValue === 2 && (
                        <FuseAnimateGroup
                            className="flex flex-wrap"
                            enter={{
                                animation: "transition.slideUpBigIn"
                            }}
                        >
                        </FuseAnimateGroup>
                    )}
                </div>
            }
            rightSidebarContent={
                <FuseAnimateGroup
                    className="w-full"
                    enter={{
                        animation: "transition.slideUpBigIn"
                    }}
                >
                    <div className="widget w-full p-12">
                        <WidgetNow/>
                    </div>
                </FuseAnimateGroup>
            }
            ref={pageLayout}
        />
    );
}

export default withReducer('teknikDashboard', reducer)(TeknikYonetimDashboard);
