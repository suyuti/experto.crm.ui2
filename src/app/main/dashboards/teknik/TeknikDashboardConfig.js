import {authRoles} from 'app/auth';
import React from 'react';

export const TeknikDashboardConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    auth: authRoles.teknik_personel,
    routes  : [
        {
            path     : '/dashboards/teknik',
            component: React.lazy(() => import('./TeknikDashboard'))
        }
    ]
};
