import {authRoles} from 'app/auth';
import React from 'react';

export const TeknikYonetimDashboardConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    auth: authRoles.teknik_yonetici,
    routes  : [
        {
            path     : '/dashboards/teknik/yonetim',
            component: React.lazy(() => import('./TeknikYonetimDashboard'))
        }
    ]
};
