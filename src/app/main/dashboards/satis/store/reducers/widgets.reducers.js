import * as Actions from '../actions';

const initialState = {
    widget2: null,
    widgetTeklifler: null,
    widgetMusteriAdaylari: null
};

const widgetsReducer = function (state = initialState, action) {
    switch (action.type) {
        case Actions.GET_WIDGET_2:
            return {
                ...state,
                widget2: { ...action.payload }

            };
        case Actions.GET_WIDGET_TEKLIFLER:
            return {
                ...state,
                widgetTeklifler: { ...action.payload }
            };
        case Actions.GET_WIDGET_MUSTERI_ADAYLARI:
            return {
                ...state,
                widgetMusteriAdaylari: { ...action.payload.data }
            };

        default:
            return state;
    }
};

export default widgetsReducer;
