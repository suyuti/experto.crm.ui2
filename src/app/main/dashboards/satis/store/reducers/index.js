import {combineReducers} from 'redux';
import satis from './satis.reducers';
import widgets from './widgets.reducers';

const reducer = combineReducers({
    satis,
    widgets
});

export default reducer;
