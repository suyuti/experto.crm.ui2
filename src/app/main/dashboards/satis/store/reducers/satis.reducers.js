import * as Actions from '../actions';

const initialState = {
    teklifler: null,
    musteriler: null
};

const satisReducer = function (state = initialState, action) {
    switch (action.type) {
        case Actions.GET_TEKLIFLER:
            return {
                ...state,
                teklifler: {...action.payload}
            }
        case Actions.GET_MUSTERILER:
            return {
                ...state,
                musteriler: {...action.payload}
            }
        default:
            return state;
    }
};

export default satisReducer;
