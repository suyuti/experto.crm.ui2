import axios from 'axios';

export const GET_TEKLIFLER  = '[SATIS DASHBOARD] GET TEKLIFLER';
export const GET_MUSTERILER = '[SATIS DASHBOARD] GET MUSTERILER';

export function getTeklifler()
{
    const request = axios.get('/api/teklif');

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_TEKLIFLER,
                payload: response.data
            })
        );
}

export function getMusteriler()
{
    const request = axios.get('/api/musteri');

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_MUSTERILER,
                payload: response.data
            })
        );
}
