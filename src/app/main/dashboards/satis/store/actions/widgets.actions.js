import axios from 'axios';

export const GET_WIDGET_2                   = '[SATIS DASHBOARD] GET WIDGET 2';
export const GET_WIDGET_TEKLIFLER           = '[SATIS DASHBOARD] GET WIDGET TEKLIFLER';
export const GET_WIDGET_MUSTERI_ADAYLARI    = '[SATIS DASHBOARD] GET MUSTERI ADAYLARI';

export function getWidget2() {
    const request = axios.get('/api/widgets/2');

    return (dispatch) =>
        request.then((response) => {
            dispatch({
                type: GET_WIDGET_2,
                payload: response.data
            })
        }
        );
}

export function getWidgetTeklifler() {
    const request = axios.get('/api/widgets/teklifler');

    return (dispatch) =>
        request.then((response) => {
            dispatch({
                type: GET_WIDGET_TEKLIFLER,
                payload: response.data
            })
        }
        );
}

export function getWidgetOnayBekleyenTeklifler() {
    const request = axios.get('/api/widgets/onaybekleyenteklifler');

    return (dispatch) =>
        request.then((response) => {
            dispatch({
                type: GET_WIDGET_TEKLIFLER,
                payload: response.data
            })
        }
        );
}

export function getWidgetMusteriAdaylari() {
    const request = axios.get('/api/widgets/widgetMusteriAdaylariData');

    return (dispatch) =>
        request.then((response) => {
            dispatch({
                type: GET_WIDGET_MUSTERI_ADAYLARI,
                payload: response.data
            })
        }
        );
}
