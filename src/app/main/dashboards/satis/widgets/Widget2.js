import React, {useEffect, useState} from 'react';
import {Icon, Typography, Paper, IconButton} from '@material-ui/core';
import * as Actions from '../store/actions'
import {useDispatch, useSelector} from 'react-redux';

function Widget2(props)
{
    const [ isLoading, setIsLoading ] = useState(true)
    const dispatch = useDispatch();
    const widget2 = useSelector(({satisDashboard}) => satisDashboard.widgets.widget2);

    useEffect(() => {
        const load = async () => {
            try {
                await dispatch(Actions.getWidget2())
                setIsLoading(false)
            }
            catch(e) {}
        }
        load()
    }, [dispatch])

    if (isLoading) {
        return (
            <div>Loading</div>
        )
    }
    else {
        return (
            <Paper className="w-full rounded-8 shadow-none border-1">
            <div className="flex items-center justify-between px-4 pt-4">
                <Typography className="text-16 px-12">{widget2.title}</Typography>
                <IconButton aria-label="more">
                    <Icon>more_vert</Icon>
                </IconButton>
            </div>
            <div className="text-center pt-12 pb-28">
                <Typography
                    className="text-72 leading-none text-red">{widget2.data.count}</Typography>
                <Typography className="text-16" color="textSecondary">{widget2.data.label}</Typography>
            </div>
            <div className="flex items-center px-16 h-52 border-t-1">
                <Typography className="text-15 flex w-full" color="textSecondary">
                    <span className="truncate">{widget2.data.extra.label}</span>
                    :
                    <b className="px-8">{widget2.data.extra.count}</b>
                </Typography>
            </div>
        </Paper>
    );
}
}

export default React.memo(Widget2);
