import React, {useEffect} from 'react';
import {Button, Icon, Table, TableHead, TableCell, TableRow, Typography, Paper, TableBody} from '@material-ui/core';
import clsx from 'clsx';
import {useDispatch, useSelector, connect} from 'react-redux';
import * as Actions from '../store/actions'
import moment from 'moment';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';

function WidgetOnayBekleyenTeklifler(props)
{
    const dispatch          = useDispatch();
    const widgetTeklifler   = useSelector(({satisDashboard}) => satisDashboard.widgets.widgetTeklifler);

    useEffect(() => {
        function loadTeklifler() {
            dispatch(Actions.getWidgetOnayBekleyenTeklifler())
        }
        loadTeklifler()
    }, [dispatch])

    if (!widgetTeklifler) {
        return (
            <div>Loading</div>
        )
    }

    function handleClick(event, teklifId) {
        props.history.push('/teklif/' + teklifId);
    }

    if (!widgetTeklifler) {
        return (
            <div>Loading</div>
        )
    }

    return (
        <Paper className="w-full rounded-8 shadow-none border-1">
            <div className="flex items-center justify-between px-16 h-64 border-b-1">
                <Typography className="text-16">Onay Bekleyen Teklifler</Typography>
            </div>
            <div className="table-responsive">
                <Table className="w-full min-w-full" size="small">
                    <TableHead>
                        <TableRow>
                            <TableCell className="whitespace-no-wrap">
                                Firma
                            </TableCell>
                            <TableCell className="whitespace-no-wrap">
                                Urun
                            </TableCell>
                            <TableCell className="whitespace-no-wrap">
                                Olusturma tarihi
                            </TableCell>
                            <TableCell className="whitespace-no-wrap">
                                Olusturan
                            </TableCell>
                            <TableCell className="whitespace-no-wrap">
                                Onay
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {widgetTeklifler.data.map(row => (
                            <TableRow hover key={row._id} onClick={event => handleClick(event, row._id)}>
                                <TableCell key="firma"  >{row.Firma.FirmaMarkasi}</TableCell>
                                <TableCell>{row.Urun.Adi}</TableCell>
                                <TableCell>{moment(row.VerilisTarihi).format("DD.MM.YYYY")}</TableCell>
                                <TableCell>{row.MusteriTemsilcisi.Adi}</TableCell>
                                <TableCell><Button                                 variant="contained"
                                color="secondary"
>Onay</Button></TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </div>
        </Paper>
    );
}

export default compose(
    withRouter,
    React.memo
)(WidgetOnayBekleyenTeklifler)

//export default React.memo(
//    connect(withRouter)
//    (WidgetOnayBekleyenTeklifler));
