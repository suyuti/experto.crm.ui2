import React, {useEffect} from 'react';
import {Card, Icon, Typography} from '@material-ui/core';
import {useTheme} from '@material-ui/styles';
import {Bar} from 'react-chartjs-2';
import {useDispatch, useSelector} from 'react-redux';
import * as Actions from '../store/actions'

function WidgetMusteriAdaylari(props)
{
    const theme                 = useTheme();
    const dispatch              = useDispatch();
    const widgetMusteriAdaylari = useSelector(({satisDashboard}) => satisDashboard.widgets.widgetMusteriAdaylari);

    useEffect(() => {
        function loadData() {
            dispatch(Actions.getWidgetMusteriAdaylari())
        }
        loadData()
    }, [dispatch])

    if (!widgetMusteriAdaylari) {
        return (
            <div>Loading</div>
        )
    }

    console.log(widgetMusteriAdaylari)


    return (
        <Card className="w-full rounded-8 shadow-none border-1">

            <div className="p-16 pb-0 flex flex-row flex-wrap items-end">

                <div className="">
                    <Typography className="h3" color="textSecondary">Müşteri Adayları</Typography>
                    <Typography className="text-56 font-300 leading-none mt-8">
                        {widgetMusteriAdaylari.value}
                    </Typography>
                </div>

                <div className="py-4 text-16 flex flex-row items-center">
                    <div className="flex flex-row items-center">
                        {widgetMusteriAdaylari.ofTarget > 0 && (
                            <Icon className="text-green">trending_up</Icon>
                        )}
                        {widgetMusteriAdaylari.ofTarget < 0 && (
                            <Icon className="text-red">trending_down</Icon>
                        )}
                        <Typography className="mx-4">{widgetMusteriAdaylari.ofTarget}%</Typography>
                    </div>
                    <Typography className="whitespace-no-wrap">hedef</Typography>
                </div>

            </div>

            <div className="h-96 w-100-p">
                <Bar data={{
                    labels  : widgetMusteriAdaylari.labels,
                    datasets: widgetMusteriAdaylari.datasets.map(obj => ({
                        ...obj,
                        borderColor    : theme.palette.secondary.main,
                        backgroundColor: theme.palette.secondary.main
                    }))
                }} options={widgetMusteriAdaylari.options}/>
            </div>
        </Card>
    );
}

export default React.memo(WidgetMusteriAdaylari);
