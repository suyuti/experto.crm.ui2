import {authRoles} from 'app/auth';
import React from 'react';

export const SatisYonetimDashboardConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    auth: authRoles.satis_yonetici,
    routes  : [
        {
            path     : '/dashboards/satis/yonetim',
            component: React.lazy(() => import('./SatisYonetimDashboard'))
        }
    ]
};
