import {authRoles} from 'app/auth';
import React from 'react';

export const SatisDashboardConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    auth: authRoles.satis_personel,
    routes  : [
        {
            path     : '/dashboards/satis',
            component: React.lazy(() => import('./SatisDashboard'))
        }
    ]
};
