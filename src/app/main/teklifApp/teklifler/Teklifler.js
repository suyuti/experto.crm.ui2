import React from 'react';
import {FusePageCarded} from '@fuse';
import withReducer from 'app/store/withReducer';
import TekliflerTable from './TekliflerTable';
import TekliflerHeader from './TekliflerHeader';
import reducer from '../store/reducers';

function Teklifler()
{
    return (
        <FusePageCarded
            classes={{
                content: "flex",
                header : "min-h-72 h-72 sm:h-136 sm:min-h-136"
            }}
            header={
                <TekliflerHeader/>
            }
            content={
                <TekliflerTable/>
            }
            innerScroll
        />
    );
}

export default withReducer('teklifApp', reducer)(Teklifler);
