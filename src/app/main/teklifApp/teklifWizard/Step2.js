import React from 'react';
import {Select, OutlinedInput, MenuItem} from '@material-ui/core';
import {useDispatch, useSelector} from 'react-redux';
import withReducer from 'app/store/withReducer';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';


function TeklifStep2(props) {
    const dispatch  = useDispatch()
    const teklif    = useSelector(({teklifApp}) => teklifApp.teklif.teklif);
    const urunler   = useSelector(({teklifApp}) => teklifApp.teklif.urunler)

    function _handleChange(e) {
        dispatch(Actions.teklifUrunSec(e.target.value))
    }

    return (
        <Select
            fullWidth
            required
            labelId="sektor-select-outlined-label"
            className="mt-8 mb-16"
            value={teklif.Urun ? teklif.Urun._id : ''}
            onChange={_handleChange}
            input={
                <OutlinedInput
                    labelWidth={("category".length * 9)}
                    name="urun"
                    id="urun"
                />
            }
        >
            {
                urunler.map(urun => (
                    <MenuItem value={urun._id} key={urun._id}>{urun.Adi}</MenuItem>
                ))}
        </Select>
    );

}

export default withReducer('teklifApp', reducer)(TeklifStep2);
