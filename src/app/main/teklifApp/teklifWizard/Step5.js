import React from 'react';
import { TextField, Icon, Select, MenuItem, OutlinedInput } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import withReducer from 'app/store/withReducer';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { DatePicker } from "@material-ui/pickers";
import moment from 'moment';
const _ = require('lodash')

function TeklifStep5(props) {
    const dispatch = useDispatch()
    const teklif = useSelector(({ teklifApp }) => teklifApp.teklif.teklif);
    //let termin = moment(new Date(), 'MM/DD/YYYY').add(15, 'days');

    function handleChange(e) {
        dispatch(Actions.teklifSureBelirle(e.target.value))
    }


    return (
        <div>
            <div className="flex">
                <div className="min-w-48 pt-20">
                    <Icon color="action">account_circle</Icon>
                </div>

                <Select
                    fullWidth
                    required
                    labelId="sektor-select-outlined-label"
                    className="mt-8 mb-16"
                    value={teklif.sure ? teklif.sure : ''}
                    onChange={handleChange}
                    input={
                        <OutlinedInput
                            labelWidth={("category".length * 9)}
                            name="urun"
                            id="urun"
                        />
                    }
                >
                    {
                        _.range(32).map(i => (
                            <MenuItem value={i + 1} key={i + 1}>{i + 1}</MenuItem>
                        ))
                    }
                </Select>
            </div>
            <div className="flex">
                <div className="min-w-48 pt-20">
                    <Icon color="action">account_circle</Icon>
                </div>
                <DatePicker
                    label="Teklif geçerlilik"
                    inputVariant="outlined"
                    value={teklif.termin}
                    onChange={date => {
                        dispatch(Actions.teklifTerminBelirle(date))
                    }}
                    className="mt-8 mb-16 w-full"
                    maxDate={teklif.termin}
                    minDate={new Date()}
                />

            </div>
        </div>
    );

}

export default withReducer('teklifApp', reducer)(TeklifStep5);
