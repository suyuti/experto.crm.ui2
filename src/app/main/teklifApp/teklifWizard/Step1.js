import React from 'react';
import {Select, OutlinedInput, MenuItem} from '@material-ui/core';
import {useDispatch, useSelector} from 'react-redux';
import withReducer from 'app/store/withReducer';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';


function TeklifStep1(props) {
    const dispatch      = useDispatch()
    const teklif        = useSelector(({teklifApp}) => teklifApp.teklif.teklif);
    const musteriler    = useSelector(({teklifApp}) => teklifApp.teklif.musteriler)


    function _handleChange(e) {
        dispatch(Actions.teklifMusteriSec(e.target.value))
    }

    return (
        <Select
            fullWidth
            required
            labelId="sektor-select-outlined-label"
            className="mt-8 mb-16"
            value={teklif.Firma ? teklif.Firma._id : ''}
            onChange={_handleChange}
            input={
                <OutlinedInput
                    //labelWidth={("category".length * 9)}
                    //name="musteri"
                    id="firma"
                />
            }
        >
            {
                musteriler.map(musteri => (
                    <MenuItem value={musteri._id} key={musteri._id}>{musteri.FirmaMarkasi}</MenuItem>
                ))}
        </Select>
    );
}

export default withReducer('teklifApp', reducer)(TeklifStep1);
