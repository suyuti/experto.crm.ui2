import React from 'react';
import { TextField, Icon } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import withReducer from 'app/store/withReducer';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';

function TeklifStep4(props) {
    const dispatch      = useDispatch()
    const teklif        = useSelector(({ teklifApp }) => teklifApp.teklif.teklif);
    //const ilaveKosullar = useSelector(({ teklifApp }) => teklifApp.teklif.ilaveKosullar);
    //const iade          = useSelector(({ teklifApp }) => teklifApp.teklif.iade);

    function handleChange(e) {
        dispatch(Actions.teklifIlaveKosul(e.target.value))
    }
    function handleIadeChange(e, checked) {
        dispatch(Actions.teklifSetIade(checked))
    }

    return (
        <div>
            <div className="flex">
                <div className="min-w-48 pt-20">
                    <Icon color="action">account_circle</Icon>
                </div>

                <TextField
                    className="mb-24"
                    label="İlave Koşullar"
                    id="IlaveKodullar"
                    name="IlaveKosullar"
                    value={teklif.ilaveKosullar}
                    onChange={handleChange}
                    variant="outlined"
                    rows={3}
                    multiline
                    fullWidth
                />
            </div>
            <div className="flex">
                <FormControlLabel
                    value="start"
                    control={<Switch
                        checked={teklif.iade}
                        onChange={handleIadeChange}
                        value={teklif.iade}
                        inputProps={{ 'aria-label': 'secondary checkbox' }}
                    />
                    }
                    label="İadeli mi?"
                    labelPlacement="start"
                />
            </div>
        </div>
    );

}

export default withReducer('teklifApp', reducer)(TeklifStep4);
