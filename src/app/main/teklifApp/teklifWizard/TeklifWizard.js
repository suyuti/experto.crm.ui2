import React, { useEffect } from 'react';
import { Button, Icon, Typography } from '@material-ui/core';
import { orange } from '@material-ui/core/colors';
import { makeStyles, useTheme } from '@material-ui/styles';
import { FuseAnimate, FusePageCarded, FuseLoading } from '@fuse';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import withReducer from 'app/store/withReducer';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Paper from '@material-ui/core/Paper';
import Step1 from './Step1'
import Step2 from './Step2'
import Step3 from './Step3'
import Step4 from './Step4'
import Step5 from './Step5'
import TeklifOzet from '../teklif/TeklifOzet'

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
    },
    button: {
        marginTop: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    actionsContainer: {
        marginBottom: theme.spacing(2),
    },
    resetContainer: {
        padding: theme.spacing(3),
    },
    productImageFeaturedStar: {
        position: 'absolute',
        top: 0,
        right: 0,
        color: orange[400],
        opacity: 0
    },
    productImageUpload: {
        transitionProperty: 'box-shadow',
        transitionDuration: theme.transitions.duration.short,
        transitionTimingFunction: theme.transitions.easing.easeInOut,
    },
    productImageItem: {
        transitionProperty: 'box-shadow',
        transitionDuration: theme.transitions.duration.short,
        transitionTimingFunction: theme.transitions.easing.easeInOut,
        '&:hover': {
            '& $productImageFeaturedStar': {
                opacity: .8
            }
        },
        '&.featured': {
            pointerEvents: 'none',
            boxShadow: theme.shadows[3],
            '& $productImageFeaturedStar': {
                opacity: 1
            },
            '&:hover $productImageFeaturedStar': {
                opacity: 1
            }
        }
    }
}));

function getSteps() {
    return ['Müşteri Seçin', 
            'Ürün Seçin', 
            'Fiyat ve Şartları belirleyin',
            'varsa İlave koşullar',
            'Süre belirleyin'];
}

function getStepContent(step) {
    switch (step) {
        case 0:
            return <Step1 />
        case 1:
            return <Step2 />
        case 2:
            return <Step3 />
        case 3:
            return <Step4 />
        case 4:
            return <Step5 />
        default:
            return 'Unknown step';
    }
}

function TeklifWizard(props) {
    const dispatch      = useDispatch();
    const user          = useSelector(({ auth }) => auth.user);
    const teklif        = useSelector(({ teklifApp }) => teklifApp.teklif.teklif);
    const error        = useSelector(({ teklifApp }) => teklifApp.teklif.error);
    const musteriler    = useSelector(({ teklifApp }) => teklifApp.teklif.musteriler)
    
    const theme = useTheme();
    const [activeStep, setActiveStep] = React.useState(0);
    const [bitti, setBitti] = React.useState(false);
    const classes = useStyles(props);
    
    const handleNext = () => {
        if (activeStep == steps.length-1) {
            dispatch(Actions.teklifHesapla())
            setBitti(true)
        }
        setActiveStep(prevActiveStep => prevActiveStep + 1);
    };

    const handleBack = () => {
        setActiveStep(prevActiveStep => prevActiveStep - 1);
    };

    const handleReset = () => {
        setBitti(false)
        dispatch(Actions.newTeklif(user._id))
        setActiveStep(0);
    };

    const handleTeklifTamam = () => {
        dispatch(Actions.saveTeklif(teklif))
    }

    useEffect(() => {
        dispatch(Actions.getMusteriler());
        dispatch(Actions.getUrunler());
        dispatch(Actions.getUserData());
    }, [dispatch, props.match.params]);

    useEffect(() => {
        function updateProductState() {
            dispatch(Actions.newTeklif(user._id))
            dispatch(Actions.getUserData());
        }

        updateProductState();
    }, [dispatch, props.match.params]);


    function canBeSubmitted() {
        return ( // TODO
            false
            //form.name.length > 0 &&
            //!_.isEqual(teklif.data, form)
        );
    }

    //if (!teklif ) {//|| (teklif && props.match.params.teklifId !== teklif.id)) && props.match.params.teklifId !== 'new') {
    //    return <FuseLoading />;
   // }

    if (!musteriler) {
        return <FuseLoading />;
    }

    const steps = getSteps(musteriler);


    if (bitti) {
        return <TeklifOzet iptalHandler={handleReset} tamamHandler={handleTeklifTamam} />
    }

    return (
        <FusePageCarded
            classes={{
                toolbar: "p-0",
                header: "min-h-72 h-72 sm:h-136 sm:min-h-136"
            }}
            header={
                true && (
                    <div className="flex flex-1 w-full items-center justify-between">

                        <div className="flex flex-col items-start max-w-full">

                            <FuseAnimate animation="transition.slideRightIn" delay={300}>
                                <Typography className="normal-case flex items-center sm:mb-12" component={Link} role="button" to="/teklifler/all" color="inherit">
                                    <Icon className="text-20">{theme.direction === "ltr" ? "arrow_back" : "arrow_forward"}</Icon>
                                    <span className="mx-4">Teklifler</span>
                                </Typography>
                            </FuseAnimate>

                            <div className="flex items-center max-w-full">
                                {/*<FuseAnimate animation="transition.expandIn" delay={300}>
                                    {form.images.length > 0 && form.featuredImageId ? (
                                        <img className="w-32 sm:w-48 rounded" src={_.find(form.images, {id: form.featuredImageId}).url} alt={form.name}/>
                                    ) : (
                                        <img className="w-32 sm:w-48 rounded" src="assets/images/ecommerce/product-image-placeholder.png" alt={form.name}/>
                                    )}
                                </FuseAnimate>
                                    */}
                                <div className="flex flex-col min-w-0 mx-8 sm:mc-16">
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography className="text-16 sm:text-20 truncate">
                                            {'Teklif Sihirbazı'}
                                        </Typography>
                                    </FuseAnimate>
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography variant="caption">Yeni teklif oluşturma</Typography>
                                    </FuseAnimate>
                                </div>
                            </div>
                        </div>
                        <FuseAnimate animation="transition.slideRightIn" delay={300}>
                            <Button
                                className="whitespace-no-wrap normal-case"
                                variant="contained"
                                color="secondary"
                                disabled={!canBeSubmitted()}
                                //onClick={() => dispatch(Actions.saveTeklif(null))}
                            >
                                Save
                            </Button>
                        </FuseAnimate>
                    </div>
                )
            }
            content={
                <div className={classes.root}>
                    <Stepper activeStep={activeStep} orientation="vertical">
                        {steps.map((label, index) => (
                            <Step key={label}>
                                <StepLabel>{label}</StepLabel>
                                <StepContent>
                                    <Typography>{getStepContent(index)}</Typography>
                                    <div className={classes.actionsContainer}>
                                        <div>
                                            <Button
                                                disabled={activeStep === 0}
                                                onClick={handleBack}
                                                className={classes.button}
                                            >
                                                Geri
                                            </Button>
                                            <Button
                                                disabled={      (activeStep === 0 && !teklif.Firma) 
                                                            ||  (activeStep === 1 && !teklif.Urun) 
                                                            ||  (activeStep === 2 && error ? Object.keys(error).length > 0 : false)}
                                                variant="contained"
                                                color="primary"
                                                onClick={handleNext}
                                                className={classes.button}
                                            >
                                                {activeStep === steps.length - 1 ? 'Bitti' : 'İleri'}
                                            </Button>
                                        </div>
                                    </div>
                                </StepContent>
                            </Step>
                        ))}
                    </Stepper>
                    {activeStep === steps.length && (
                        <Paper square elevation={0} className={classes.resetContainer}>
                            <Typography>Teklif tamamlandı</Typography>
                            <Button onClick={handleReset} className={classes.button}>
                                Başa dön
                            </Button>
                            <Button onClick={handleReset} className={classes.button}>
                                Onaya Gönder
                            </Button>
                        </Paper>
                    )}
                </div>}
            innerScroll
        />
    )
}

export default withReducer('teklifApp', reducer)(TeklifWizard);
