import React from 'react';
import { TextField, InputAdornment, Icon } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import withReducer from 'app/store/withReducer';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';


function TeklifStep3(props) {
    const dispatch      = useDispatch()
    const teklif        = useSelector(({ teklifApp }) => teklifApp.teklif.teklif);
    const error         = useSelector(({ teklifApp }) => teklifApp.teklif.error);
    const user          = useSelector(({ auth }) => auth.user);

    function _handleChange(e) {
        let val = e.target.value.length === 0 ? 0 : e.target.value
        dispatch(Actions.teklifFiyatDegistir(e.target.name, val, user.IndirimOrani))
    }

    return (
        <div>
            {teklif.Urun
                && teklif.Urun.OnOdemeTutari != null
                &&
                <div className="flex">
                    <div className="min-w-48 pt-20">
                        <Icon color="action">account_circle</Icon>
                    </div>
                    <TextField
                        error={error && error.OnOdemeTutari}
                        helperText= {error?error.OnOdemeTutari: null}
                        className="mb-24"
                        label="Ön ödeme fiyatı"
                        autoFocus
                        id="OnOdemeTutari"
                        name="OnOdemeTutari"
                        value={teklif.OnOdemeTutari}
                        onChange={_handleChange}
                        variant="outlined"
                        required
                        fullWidth
                        InputProps={{
                            startAdornment: <InputAdornment position="start">TL</InputAdornment>
                        }}
                    />
                </div>
            }
            {teklif.Urun
                && teklif.Urun.SabitTutar != null
                &&
                <div className="flex">
                    <div className="min-w-48 pt-20">
                        <Icon color="action">account_circle</Icon>
                    </div>

                    <TextField
                        error={error && error.SabitTutar}
                        helperText= {error?error.SabitTutar: null}
                        className="mb-24"
                        label="Sabit ödeme"
                        id="SabitTutar"
                        name="SabitTutar"
                        value={teklif.SabitTutar}
                        onChange={_handleChange}
                        variant="outlined"
                        required
                        fullWidth
                        InputProps={{
                            startAdornment: <InputAdornment position="start">TL</InputAdornment>
                        }}
                    />
                </div>
            }

            {teklif.Urun
                && teklif.Urun.RaporBasiOdemeTutari != null
                &&
                <div className="flex">
                    <div className="min-w-48 pt-20">
                        <Icon color="action">account_circle</Icon>
                    </div>

                    <TextField
                        error={error && error.RaporBasiOdeme}
                        helperText= {error?error.RaporBasiOdeme: null}
                        className="mb-24"
                        label="Rapor başına ödeme"
                        id="RaporBasiOdeme"
                        name="RaporBasiOdeme"
                        value={teklif.RaporBasiOdeme}
                        onChange={_handleChange}
                        variant="outlined"
                        required
                        fullWidth
                        InputProps={{
                            startAdornment: <InputAdornment position="start">TL</InputAdornment>
                        }}
                    />
                </div>
            }
            {teklif.Urun
                && teklif.Urun.BasariTutari != null
                &&

                <div className="flex">
                    <div className="min-w-48 pt-20">
                        <Icon color="action">account_circle</Icon>
                    </div>

                    <TextField
                        error={error && error.BasariTutari}
                        helperText= {error?error.BasariTutari: null}
                        className="mb-24"
                        label="Başarı Ödeme"
                        id="BasariTutari"
                        name="BasariTutari"
                        value={teklif.BasariTutari}
                        onChange={_handleChange}
                        variant="outlined"
                        required
                        fullWidth
                        InputProps={{
                            startAdornment: <InputAdornment position="start">TL</InputAdornment>
                        }}
                    />
                </div>
            }
            {teklif.Urun
                && teklif.Urun.YuzdeTutari != null
                &&
                <div className="flex">
                    <div className="min-w-48 pt-20">
                        <Icon color="action">account_circle</Icon>
                    </div>

                    <TextField
                        error={error && error.YuzdeTutari}
                        helperText= {error?error.YuzdeTutari: null}
                        className="mb-24"
                        label="Yüzde Ödeme"
                        id="YuzdeTutari"
                        name="YuzdeTutari"
                        value={teklif.YuzdeTutari}
                        onChange={_handleChange}
                        variant="outlined"
                        required
                        fullWidth
                        InputProps={{
                            startAdornment: <InputAdornment position="start">TL</InputAdornment>
                        }}
                    />
                </div>
            }
        </div>
    );

}

export default withReducer('teklifApp', reducer)(TeklifStep3);
