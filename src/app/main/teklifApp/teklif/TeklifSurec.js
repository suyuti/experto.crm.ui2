import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { Button, Grid, Paper, Table, TableBody, TableCell, TableHead, TableRow, Typography, Icon } from '@material-ui/core';


const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
    },
    button: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
}));

function handleTeklifSurecAction(surec) {
    alert(surec)
}

export default function TeklifSurec() {
    const classes = useStyles();

    return (
        <div className="p-12">
            <Grid container spacing={3}>
                <Grid item xs={4}>
                    <Button variant="contained" color="primary" fullWidth className="my-4" onClick= {() => handleTeklifSurecAction('IlkToplanti')}>
                        Ilk toplanti Yap
                    </Button>
                    <Button variant="contained" color="primary" fullWidth className="my-4" onClick= {() => handleTeklifSurecAction('TeklifVer')}>
                        Teklif Ver
                    </Button>
                    <Button variant="contained" color="primary" fullWidth className="my-4" onClick= {() => handleTeklifSurecAction('OnTeknikToplanti')}>
                        Ön teknik toplantı yap
                    </Button>
                    <Button variant="contained" color="primary" fullWidth className="my-4" onClick= {() => handleTeklifSurecAction('SozlesmeGonder')}>
                        Sözleşme gönder
                    </Button>
                </Grid>
                <Grid item xs={8}>
                    <Table className="simple">
                        <TableBody>
                            <TableRow >
                                <TableCell>
                                    <Typography variant="subtitle1">Teklif oluşturuldu</Typography>
                                </TableCell>
                                <TableCell>
                                    <Icon className="text-32" color="action">check</Icon>
                                </TableCell>
                            </TableRow>
                            <TableRow >
                                <TableCell>
                                    <Typography variant="subtitle1">İlk toplantı yapıldı</Typography>
                                </TableCell>
                                <TableCell>
                                    <Icon className="text-32" color="action">check</Icon>
                                </TableCell>
                            </TableRow>
                            <TableRow >
                                <TableCell>
                                    <Typography variant="subtitle1">Teklif verildi</Typography>
                                </TableCell>
                                <TableCell>
                                    <Icon className="text-32" color="action">check</Icon>
                                </TableCell>
                            </TableRow>
                            <TableRow >
                                <TableCell>
                                    <Typography variant="subtitle1">Ön teknik toplantı yapıldı</Typography>
                                </TableCell>
                                <TableCell>
                                    <Icon className="text-32" color="action">check</Icon>
                                </TableCell>
                            </TableRow>
                            <TableRow >
                                <TableCell>
                                    <Typography variant="subtitle1">Sözleşme gönderildi</Typography>
                                </TableCell>
                                <TableCell>
                                    <Icon className="text-32" color="action">check</Icon>
                                </TableCell>
                            </TableRow>
                            <TableRow >
                                <TableCell>
                                    <Typography variant="subtitle1">Sözleşme yapıldı</Typography>
                                </TableCell>
                                <TableCell>
                                    <Icon className="text-32" color="action">check</Icon>
                                </TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                </Grid>
            </Grid>
        </div>
    );
}