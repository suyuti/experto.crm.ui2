import React, { useEffect, useState } from 'react';
import { Button, Tab, Tabs, TextField, InputAdornment, Icon, Typography } from '@material-ui/core';
import { orange } from '@material-ui/core/colors';
import { makeStyles, useTheme } from '@material-ui/styles';
import { FuseAnimate, FusePageCarded, FuseChipSelect, FuseUtils, FuseLoading } from '@fuse';
import { useForm } from '@fuse/hooks';
import { Link } from 'react-router-dom';
import clsx from 'clsx';
import _ from '@lodash';
import { useDispatch, useSelector } from 'react-redux';
import withReducer from 'app/store/withReducer';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import TeklifOzet from './TeklifOzet';
import TeklifSurec from './TeklifSurec';

const useStyles = makeStyles(theme => ({
    productImageFeaturedStar: {
        position: 'absolute',
        top: 0,
        right: 0,
        color: orange[400],
        opacity: 0
    },
    productImageUpload: {
        transitionProperty: 'box-shadow',
        transitionDuration: theme.transitions.duration.short,
        transitionTimingFunction: theme.transitions.easing.easeInOut,
    },
    productImageItem: {
        transitionProperty: 'box-shadow',
        transitionDuration: theme.transitions.duration.short,
        transitionTimingFunction: theme.transitions.easing.easeInOut,
        '&:hover': {
            '& $productImageFeaturedStar': {
                opacity: .8
            }
        },
        '&.featured': {
            pointerEvents: 'none',
            boxShadow: theme.shadows[3],
            '& $productImageFeaturedStar': {
                opacity: 1
            },
            '&:hover $productImageFeaturedStar': {
                opacity: 1
            }
        }
    }
}));

function Teklif(props) {
    const dispatch  = useDispatch();
    const teklif    = useSelector(({ teklifApp }) => teklifApp.teklif.teklif);
    const user      = useSelector(({ auth }) => auth.user);
    const theme     = useTheme();
    const classes   = useStyles(props);
    const [tabValue, setTabValue] = useState(0);
    const { form, handleChange, setForm } = useForm(null);

    useEffect(() => {
        function updateTeklif() {
            const params = props.match.params;
            const { teklifId } = params;
            dispatch(Actions.getTeklif({teklifId}))
        }
        updateTeklif();
    }, [dispatch, props.match.params]);

    function handleChangeTab(event, tabValue) {
        setTabValue(tabValue);
    }

    function canBeSubmitted() {
        return true;
    }

    //if ((!teklif || (teklif && props.match.params.teklifId !== teklif._id)) && props.match.params.teklifId !== 'new') 
    if (!teklif) {
        return <FuseLoading />;
    }


    return (
        <FusePageCarded
            classes={{
                toolbar: "p-0",
                header: "min-h-72 h-72 sm:h-136 sm:min-h-136"
            }}
            header={
                    <div className="flex flex-1 w-full items-center justify-between">

                        <div className="flex flex-col items-start max-w-full">

                            <FuseAnimate animation="transition.slideRightIn" delay={300}>
                                <Typography className="normal-case flex items-center sm:mb-12" component={Link} role="button" to="/teklifler/all" color="inherit">
                                    <Icon className="text-20">{theme.direction === "ltr" ? "arrow_back" : "arrow_forward"}</Icon>
                                    <span className="mx-4">Teklifler</span>
                                </Typography>
                            </FuseAnimate>

                            <div className="flex items-center max-w-full">
                                {/*<FuseAnimate animation="transition.expandIn" delay={300}>
                                    {form.images.length > 0 && form.featuredImageId ? (
                                        <img className="w-32 sm:w-48 rounded" src={_.find(form.images, {id: form.featuredImageId}).url} alt={form.name}/>
                                    ) : (
                                        <img className="w-32 sm:w-48 rounded" src="assets/images/ecommerce/product-image-placeholder.png" alt={form.name}/>
                                    )}
                                </FuseAnimate>
                                    */}
                                <div className="flex flex-col min-w-0 mx-8 sm:mc-16">
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography className="text-16 sm:text-20 truncate">
                                            {/*teklif.TeklifNo ? teklif.TeklifNo : ''*/}
                                        </Typography>
                                    </FuseAnimate>
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography variant="caption">Teklif Detayı</Typography>
                                    </FuseAnimate>
                                </div>
                            </div>

                        </div>
                        
                        {user.role === 'satis_yonetici' &&
                        <FuseAnimate animation="transition.slideRightIn" delay={300}>
                            <Button
                                className="whitespace-no-wrap normal-case"
                                variant="contained"
                                color="secondary"
                                //disabled={!canBeSubmitted()}
                                //onClick={() => 
                                //    {
                                //        props.match.params.musteriId === 'new' ?  dispatch(Actions.createMusteri(form)) : dispatch(Actions.saveMusteri(form))
                                //    }
                                //}
                            >
                                Onayla
                            </Button>
                        </FuseAnimate>
                        }
                    </div>
            }
            contentToolbar={
                <Tabs
                    value={tabValue}
                    onChange={handleChangeTab}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="scrollable"
                    scrollButtons="auto"
                    classes={{ root: "w-full h-64" }}
                >
                    <Tab className="h-64 normal-case" label="Genel Bilgiler" />
                    <Tab className="h-64 normal-case" label="Süreç" />
                </Tabs>
            }
            content={
                    <div className="p-16 sm:p-24 max-w-2xl">
                        {tabValue === 0 &&
                            (
                                <div>
                                    <TeklifOzet />
                                </div>
                            )}
                        {tabValue === 1 && (
                            <div>
                                <TeklifSurec />
                            </div>
                        )}
                        {tabValue === 2 && (
                            <div>
                            </div>
                        )}
                    </div>
            }
            innerScroll
        />
    )
}

export default withReducer('teklifApp', reducer)(Teklif);
