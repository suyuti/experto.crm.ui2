import React, { useState} from 'react';
import {Card, Button, CardContent, Typography, TableCell, TableRow, TableBody, TableHead, Table} from '@material-ui/core';
import {darken} from '@material-ui/core/styles/colorManipulator';
import {FuseAnimate} from '@fuse';
import clsx from 'clsx';
import {makeStyles} from '@material-ui/styles';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';

const useStyles = makeStyles(theme => ({
    root   : {
        background: 'radial-gradient(' + darken(theme.palette.primary.dark, 0.5) + ' 0%, ' + theme.palette.primary.dark + ' 80%)'
    },
    divider: {
        backgroundColor: theme.palette.getContrastText(theme.palette.primary.dark)
    },
    seller : {
        backgroundColor: theme.palette.primary.dark,
        color          : theme.palette.getContrastText(theme.palette.primary.dark),
        marginRight    : -88,
        paddingRight   : 66,
        width          : 480
    }
}));

function TeklifOzet(props) {
    const classes   = useStyles();
    const teklif    = useSelector(({ teklifApp }) => teklifApp.teklif.teklif);

    const [invoice, setInvoice] = useState(
        {
            'id'      : '5725a6802d',
            'from'    : {
                'title'  : 'Experto',
                'address': 'Tozkoparan Mah. Haldun Taner Sokak Alpaslan İş Merkezi No:25/5 Güngören-İSTANBUL',
                'phone'  : '+90 (212) 569 00 80 ',
                'email'  : 'info@experto.com.tr',
                'website': 'www.experto.com.tr'
            },
            'client'  : {
                'title'  : 'John Doe',
                'address': '9301 Wood Street Philadelphia, PA 19111',
                'phone'  : '+55 552 455 87',
                'email'  : 'johndoe@mail.com'
            },
            'number'  : 'T-202002-0001',
            'date'    : 'Jul 19, 2019',
            'dueDate' : 'Aug 24, 2019',
            'services': [
                {
                    'id'       : '1',
                    'title'    : 'Ön ödeme',
                    'detail'   : 'Bir defa alınır',
                    'unit'     : 'TL',
                    'unitPrice': '10000',
                    'quantity' : '1',
                    'total'    : '2880'
                },
                {
                    'id'       : '2',
                    'title'    : 'Aylık Ödeme',
                    'detail'   : 'Her ay alınacaktır',
                    'unit'     : 'TL',
                    'unitPrice': '4000',
                    'quantity' : '12',
                    'total'    : '48000'
                },
            ],
            'subtotal': '8445',
            'tax'     : '675.60',
            'discount': '120.60',
            'total'   : '9000'
        }
    );
    const formatter = new Intl.NumberFormat('tr-TR',
        {
            style                : 'currency',
            currency             : 'TRY',
            minimumFractionDigits: 2
        });

    console.log(teklif)

    return (
        <div className={clsx(classes.root, "flex-grow flex-shrink-0 p-0 sm:p-64 print:p-0")}>

            {invoice && (
                <FuseAnimate animation={{translateY: [0, '100%']}} duration={600}>

                    <Card className="mx-auto w-xl print:w-full print:p-8 print:shadow-none">

                        <CardContent className="p-88 print:p-0">

                            <Typography color="textSecondary" className="mb-32">
                                {moment(new Date()).format("DD.MM.YYYY")}
                            </Typography>

                            <div className="flex justify-between">

                                <div>
                                    <table className="mb-16">
                                        <tbody>
                                            <tr>
                                                <td className="pb-4">
                                                    <Typography className="font-light" variant="h6" color="textSecondary">
                                                        TEKLİF
                                                    </Typography>
                                                </td>
                                                <td className="pb-4 px-16">
                                                    <Typography className="font-light" variant="h6">
                                                        {invoice.number}
                                                    </Typography>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <Typography color="textSecondary">
                                                        Teklif Tarihi
                                                    </Typography>
                                                </td>
                                                <td className="px-16">
                                                    <Typography>
                                                    {moment(teklif.teklifTarihi).format("DD.MM.YYYY")}
                                                    </Typography>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <Typography color="textSecondary">
                                                        Geçerli Tarih
                                                    </Typography>
                                                </td>
                                                <td className="px-16">
                                                    <Typography>
                                                        {moment(teklif.termin).format("DD.MM.YYYY")}
                                                    </Typography>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <Typography color="textSecondary">{teklif.Firma.FirmaMarkasi}</Typography>

                                    {invoice.client.address && (
                                        <Typography color="textSecondary">
                                            {invoice.client.address}
                                        </Typography>
                                    )}
                                    {invoice.client.phone && (
                                        <Typography color="textSecondary">
                                            {invoice.client.phone}
                                        </Typography>
                                    )}
                                    {invoice.client.email && (
                                        <Typography color="textSecondary">
                                            {invoice.client.email}
                                        </Typography>
                                    )}
                                    {invoice.client.website && (
                                        <Typography color="textSecondary">
                                            {invoice.client.website}
                                        </Typography>
                                    )}
                                </div>

                                <div className={clsx(classes.seller, "flex items-center p-16")}>

                                    <img className="w-80" src="assets/images/logos/experto-logo-whiteblack.svg" alt="logo"/>

                                    <div className={clsx(classes.divider, "w-px mx-8 h-96 opacity-50")}/>

                                    <div className="px-8">
                                        <Typography color="inherit">{invoice.from.title}</Typography>

                                        {invoice.from.address && (
                                            <Typography color="inherit">
                                                {invoice.from.address}
                                            </Typography>
                                        )}
                                        {invoice.from.phone && (
                                            <Typography color="inherit">
                                                {invoice.from.phone}
                                            </Typography>
                                        )}
                                        {invoice.from.email && (
                                            <Typography color="inherit">
                                                {invoice.from.email}
                                            </Typography>
                                        )}
                                        {invoice.from.website && (
                                            <Typography color="inherit">
                                                {invoice.from.website}
                                            </Typography>
                                        )}
                                    </div>
                                </div>
                            </div>

                            <div className="mt-64">

                                <Table className="simple">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>
                                                İŞLEM
                                            </TableCell>
                                            <TableCell>
                                                BİRİM FİYAT
                                            </TableCell>
                                            <TableCell align="right">
                                                İNDİRİMLİ FİYAT
                                            </TableCell>
                                            <TableCell align="right">
                                                ADET
                                            </TableCell>
                                            <TableCell align="right">
                                                TOPLAM
                                            </TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                    {teklif.Urun.OnOdemeTutari &&
                                            <TableRow key={teklif.Urun._id}>
                                                <TableCell>
                                                    <Typography variant="subtitle1">Ön ödeme</Typography>
                                                </TableCell>
                                                <TableCell>
                                                    {formatter.format(teklif.Urun.OnOdemeTutari)}
                                                </TableCell>
                                                <TableCell align="right">
                                                    {formatter.format(teklif.OnOdemeTutari)}
                                                </TableCell>
                                                <TableCell align="right">
                                                    1
                                                </TableCell>
                                                <TableCell align="right">
                                                    {formatter.format(teklif.OnOdemeTutari)}
                                                </TableCell>
                                            </TableRow>
                                        }
                                        {teklif.Urun.SabitTutar &&
                                            <TableRow key={teklif.Urun._id}>
                                                <TableCell>
                                                    <Typography variant="subtitle1">Sabit ödeme</Typography>
                                                </TableCell>
                                                <TableCell>
                                                    {formatter.format(teklif.Urun.SabitTutar)}
                                                </TableCell>
                                                <TableCell align="right">
                                                    {formatter.format(teklif.SabitTutar)}
                                                </TableCell>
                                                <TableCell align="right">
                                                    1
                                                </TableCell>
                                                <TableCell align="right">
                                                    {formatter.format(teklif.SabitTutar)}
                                                </TableCell>
                                            </TableRow>
                                        }
                                        {teklif.Urun.RaporBasiOdeme &&
                                            <TableRow key={teklif.Urun._id}>
                                                <TableCell>
                                                    <Typography variant="subtitle1">Rapor başına ödeme</Typography>
                                                </TableCell>
                                                <TableCell>
                                                {formatter.format(teklif.Urun.RaporBasiOdeme)}
                                                </TableCell>
                                                <TableCell align="right">
                                                    {formatter.format(teklif.RaporBasiOdeme)}
                                                </TableCell>
                                                <TableCell align="right">
                                                    1
                                                </TableCell>
                                                <TableCell align="right">
                                                    {formatter.format(teklif.RaporBasiOdeme)}
                                                </TableCell>
                                            </TableRow>
                                        }
                                        {teklif.Urun.BasariTutari &&
                                            <TableRow key={teklif.Urun._id}>
                                                <TableCell>
                                                    <Typography variant="subtitle1">Başarı ödeme</Typography>
                                                </TableCell>
                                                <TableCell>
                                                    {formatter.format(teklif.Urun.BasariTutari)}
                                                </TableCell>
                                                <TableCell align="right">
                                                    {formatter.format(teklif.BasariTutari)}
                                                </TableCell>
                                                <TableCell align="right">
                                                    1
                                                </TableCell>
                                                <TableCell align="right">
                                                    {formatter.format(teklif.BasariTutari)}
                                                </TableCell>
                                            </TableRow>
                                        }
                                        {teklif.Urun.YuzdeOdeme &&
                                            <TableRow key={teklif.Urun._id}>
                                                <TableCell>
                                                    <Typography variant="subtitle1">Yüzde ödeme</Typography>
                                                </TableCell>
                                                <TableCell>
                                                    {formatter.format(teklif.Urun.YuzdeOdeme)}
                                                </TableCell>
                                                <TableCell align="right">
                                                    {formatter.format(teklif.YuzdeOdeme)}
                                                </TableCell>
                                                <TableCell align="right">
                                                    1
                                                </TableCell>
                                                <TableCell align="right">
                                                    {formatter.format(teklif.YuzdeOdeme)}
                                                </TableCell>
                                            </TableRow>
                                        }
                                    </TableBody>
                                </Table>

                                <Table className="simple mt-32">
                                    <TableBody>
                                        <TableRow>
                                            <TableCell>
                                                <Typography className="font-medium" variant="subtitle1" color="textSecondary">Ara Toplam</Typography>
                                            </TableCell>
                                            <TableCell align="right">
                                                <Typography className="font-medium" variant="subtitle1" color="textSecondary">
                                                    {formatter.format(teklif.AraToplam)}
                                                </Typography>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>
                                    <Typography className="font-medium" variant="subtitle1" color="textSecondary">KDV %{teklif.KDVOrani}</Typography>
                                            </TableCell>
                                            <TableCell align="right">
                                                <Typography className="font-medium" variant="subtitle1" color="textSecondary">
                                                    {formatter.format(teklif.KDV)}
                                                </Typography>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>
                                                <Typography className="font-medium" variant="subtitle1" color="textSecondary">İndirim</Typography>
                                            </TableCell>
                                            <TableCell align="right">
                                                <Typography className="font-medium" variant="subtitle1" color="textSecondary">
                                                    {formatter.format(teklif.Indirim)}
                                                </Typography>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>
                                                <Typography className="font-light" variant="h4" color="textSecondary">Toplam</Typography>
                                            </TableCell>
                                            <TableCell align="right">
                                                <Typography className="font-light" variant="h4" color="textSecondary">
                                                    {formatter.format(teklif.Toplam)}
                                                </Typography>
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>

                            </div>

                            <div className="mt-96">

                                <Typography className="mb-24 print:mb-12" variant="body1">Please pay within 15 days. Thank you for your business.</Typography>

                                <div className="flex">

                                    <div className="flex-shrink-0">
                                        <img className="w-128" src="assets/images/logos/experto-logo.png" alt="logo"/>
                                    </div>

                                    <Typography className="font-medium mb-64 px-24" variant="caption" color="textSecondary">
                                        In condimentum malesuada efficitur. Mauris volutpat placerat auctor. Ut ac congue dolor. Quisque
                                    </Typography>
                                </div>
                            </div>
                        </CardContent>
                        <div className="flex justify-center pb-32">
                            <Button variant="contained" color="primary" className="w-128" onClick={props.iptalHandler}>İptal</Button>
                            <Button variant="contained" color="secondary" className="w-128" onClick={props.tamamHandler}>Tamam</Button>
                        </div>
                    </Card>
                </FuseAnimate>
            )}
        </div>
    )
}

export default TeklifOzet;