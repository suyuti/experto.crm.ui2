import React from 'react';
import {authRoles} from 'app/auth';
import _ from '@lodash';


export const TeklifAppConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    auth    : _.union(  authRoles.superadmin,
                        authRoles.admin,
                        authRoles.yonetici, 
                        authRoles.satis_yonetici, 
                        authRoles.satis_personel),
    routes  : [
        {
            path     : '/teklif/new',
            component: React.lazy(() => import('./teklifWizard/TeklifWizard'))
        },
        {
            path     : '/teklif/:teklifId/:teklifHandle?',
            component: React.lazy(() => import('./teklif/Teklif'))
        },
        {
            path     : '/teklifler/all',
            component: React.lazy(() => import('./teklifler/Teklifler'))
        },
    ]
};

