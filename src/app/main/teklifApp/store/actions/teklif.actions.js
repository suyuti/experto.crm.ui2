import axios from 'axios';
import {showMessage} from 'app/store/actions/fuse';

export const YENI_TEKLIF                = '[TEKLIF APP] YENI TEKLIF';
export const GET_TEKLIF                 = '[TEKLIF APP] GET TEKLIF';
export const SAVE_TEKLIF                = '[TEKLIF APP] SAVE TEKLIF';
export const GET_TEKLIFLER              = '[TEKLIF APP] GET TEKLIFLER';
export const SET_TEKLIFLER_SEARCH_TEXT  = '[TEKLIF APP] SET TEKLIFLER SEARCH TEXT';
export const TEKLIF_GET_MUSTERILER      = '[TEKLIF APP] TEKLIF GET MUSTERILER';
export const TEKLIF_MUSTERI_SEC         = '[TEKLIF APP] TEKLIF MUSTERI SEC';
export const TEKLIF_GET_URUNLER         = '[TEKLIF APP] TEKLIF GET URUNLER';
export const TEKLIF_URUN_SEC            = '[TEKLIF APP] TEKLIF URUN SEC';
export const TEKLIF_FIYAT_DEGISTIR      = '[TEKLIF APP] TEKLIF FIYAT DEGISTIR';
export const TEKLIF_SET_ILAVE_KOSUL     = '[TEKLIF APP] TEKLIF SET ILAVE KOSUL';
export const TEKLIF_SET_IADE            = '[TEKLIF APP] TEKLIF SET IADE';
export const TEKLIF_HESAPLA             = '[TEKLIF APP] TEKLIF HESAPLA';
export const TEKLIF_SET_SURE            = '[TEKLIF APP] TEKLIF SET SURE';
export const TEKLIF_SET_TERMIN          = '[TEKLIF APP] TEKLIF SET TERMIN';


export function getTeklif(params)
{
    const request = axios.get('/api/teklif', {params});

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_TEKLIF,
                payload: response.data
            })
        );
}

export function saveTeklif(data)
{

    let teklif = {
        MusteriTemsilcisi   : data.MusteriTemsilcisi,
        Urun                : data.Urun._id,
        Firma               : data.Firma._id,
        KapanisTarihi       : data.termin,
        OnOdemeTutari       : data.OnOdemeTutari || 0,
        RaporBasiOdemeTutari: data.RaporBasiOdemeTutari || 0,
        YuzdeTutari         : data.YuzdeTutari || 0,
        SabitTutar          : data.SabitTutar || 0,
        BasariTutari        : data.BasariTutari || 0,
        SartlarVeKosullar   : data.SartlarVeKosullar || '',
        IadeliMi            : data.iade || false,
        AraToplam           : data.AraToplam,
        KDVOran             : data.KDVOran,
        KDV                 : data.KDV,
        Indirim             : data.Indirim,
        Toplam              : data.Toplam,
        ProjeSure           : data.sure || 0   
    }

    const request = axios.post('/api/teklif', teklif);

    return (dispatch) =>
        request.then((response) => {

                dispatch(showMessage({message: 'Teklif kaydedildi'}));

                return dispatch({
                    type   : SAVE_TEKLIF,
                    payload: response.data
                })
            }
        );
}

export function getTeklifler()
{
    const request = axios.get('/api/teklif');

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_TEKLIFLER,
                payload: response.data
            })
        );
}

export function setTekliflerSearchText(event)
{
    return {
        type      : SET_TEKLIFLER_SEARCH_TEXT,
        searchText: event.target.value
    }
}

export function getMusteriler()
{
    const request = axios.get('/api/musteri');

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : TEKLIF_GET_MUSTERILER,
                payload: response.data
            })
        );
}

export function teklifMusteriSec(musteriId) {
    return {
        type: TEKLIF_MUSTERI_SEC,
        musteriId
    }
}

export function getUrunler()
{
    const request = axios.get('/api/urun');

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : TEKLIF_GET_URUNLER,
                payload: response.data
            })
        );
}

export function teklifUrunSec(urunId) {
    return {
        type: TEKLIF_URUN_SEC,
        urunId
    }
}

export function teklifFiyatDegistir(fiyatTipi, fiyat, indirimOrani) {
    return {
        type: TEKLIF_FIYAT_DEGISTIR,
        fiyatTipi,
        fiyat,
        indirimOrani
    }
}

export function teklifIlaveKosul(kosul) {
    return {
        type: TEKLIF_SET_ILAVE_KOSUL,
        ilaveKosullar: kosul
    }
}

export function teklifSetIade(iade) {
    return {
        type: TEKLIF_SET_IADE,
        iade: iade
    }
}

export function newTeklif(userId)
{
    return {
        type   : YENI_TEKLIF,
        userId
    }
}

export function teklifHesapla()
{
    return {
        type   : TEKLIF_HESAPLA,
    }
}

export function teklifSureBelirle(sure)
{
    return {
        type   : TEKLIF_SET_SURE,
        sure
    }
}

export function teklifTerminBelirle(termin)
{
    return {
        type   : TEKLIF_SET_TERMIN,
        termin
    }
}
