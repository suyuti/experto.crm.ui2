import axios from 'axios';

export const GET_USER_DATA = '[TEKLIF APP] GET USER DATA';

export function getUserData(userId)
{
    const request = axios.get('/api/user/' + userId);

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_USER_DATA,
                payload: response.data
            })
        );
}

