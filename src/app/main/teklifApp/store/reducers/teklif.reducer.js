import * as Actions from '../actions';
import moment from 'moment';
const _ = require('lodash')

const initialState = {
    musteriler: null,
    urunler: null,
    teklif: null,
    teklifler: [],
    searchText: '',
    error: null
};

const teklifReducer = function (state = initialState, action) {
    switch (action.type) {
        case Actions.GET_TEKLIF:
            {
                return {
                    ...state,
                    teklif: action.payload.data
                };
            }
        case Actions.SAVE_TEKLIF:
            {
                return {
                    ...state,
                    teklif: action.payload.data
                };
            }
        case Actions.TEKLIF_GET_MUSTERILER:
            return {
                ...state,
                musteriler: action.payload.data
            };
        case Actions.TEKLIF_MUSTERI_SEC:
            {
                const musteriId = action.musteriId
                const musteriler = [...state.musteriler]
                let seciliMusteri = musteriler.find(item => item._id === musteriId)

                return {
                    ...state,
                    teklif: {...state.teklif, Firma:seciliMusteri},
                    seciliMusteri: seciliMusteri
                };
            }
        case Actions.TEKLIF_GET_URUNLER:
            return {
                ...state,
                urunler: action.payload.data
            };
        case Actions.TEKLIF_URUN_SEC:
            {
                const urunId = action.urunId
                const urunler = [...state.urunler]
                let seciliUrun = urunler.find(item => item._id === urunId)
                let tempUrun = _.cloneDeep(seciliUrun)
                return {
                    ...state,
                    teklif: {
                        ...state.teklif, 
                        OnOdemeTutari: seciliUrun.OnOdemeTutari,
                        SabitTutar: seciliUrun.SabitTutar,
                        RaporBasiOdeme: seciliUrun.RaporBasiOdeme,
                        BasariTutari: seciliUrun.BasariTutari,
                        YuzdeTutari: seciliUrun.YuzdeTutari,
                        //tempUrun:tempUrun, 
                        Urun: seciliUrun,
                    },
                    error: null
                };
            }
        case Actions.GET_TEKLIFLER:
            {
                return {
                    ...state,
                    teklifler: action.payload.data
                };
            }
        case Actions.SET_TEKLIFLER_SEARCH_TEXT:
            {
                return {
                    ...state,
                    searchText: action.searchText
                };
            }

        case Actions.TEKLIF_FIYAT_DEGISTIR: 
            {
                //let tempUrun = {...state.teklif.tempUrun}
                let teklif = {...state.teklif}
                let seciliUrun = {...state.teklif.Urun}
                let error = {...state.error}
                //let urun = {...state.urun}
                let fiyat = parseInt(action.fiyat)
                let oran = action.indirimOrani;

                if (isNaN(fiyat)) {
                    return {...state}
                }

                if (action.fiyatTipi === 'OnOdemeTutari') {
                    if (fiyat > seciliUrun.OnOdemeTutari || fiyat < (seciliUrun.OnOdemeTutari * ((100 - oran)/100))) {
                        error['OnOdemeTutari'] =  'Ön Ödeme fiyatı hatalı'
                    }
                    else {
                        delete error['OnOdemeTutari']
                        //error=null
                    }
                    teklif.OnOdemeTutari = fiyat
                }
                else if (action.fiyatTipi === 'SabitTutar') {
                    if (fiyat > seciliUrun.SabitTutar || fiyat < (seciliUrun.SabitTutar * ((100 - oran)/100))) {
                        error['SabitTutar'] =  'Sabit Tutar fiyatı hatalı'
                    }
                    else {
                        delete error['SabitTutar']
                    }
                    teklif.SabitTutar = fiyat
                }
                else if (action.fiyatTipi === 'RaporBasiOdeme') {
                    if (fiyat > seciliUrun.RaporBasiOdeme || fiyat < (seciliUrun.RaporBasiOdeme * ((100 - oran)/100))) {
                        error['RaporBasiOdeme'] =  'Rapor Basi Odeme fiyatı hatalı'
                    }
                    else {
                        delete error['RaporBasiOdeme']
                    }
                    teklif.RaporBasiOdeme = fiyat
                }
                else if (action.fiyatTipi === 'BasariTutari') {
                    if (fiyat > seciliUrun.BasariTutari || fiyat < (seciliUrun.BasariTutari * ((100 - oran)/100))) {
                        error['BasariTutari'] =  'Basari Odeme fiyatı hatalı'
                    }
                    else {
                        delete error['BasariTutari']
                    }
                    teklif.BasariTutari = fiyat
                }
                else if (action.fiyatTipi === 'YuzdeOdeme') {
                    if (fiyat > seciliUrun.YuzdeOdeme || fiyat < (seciliUrun.YuzdeOdeme * ((100 - oran)/100))) {
                        error['YuzdeOdeme'] =  'Yuzde Odeme fiyatı hatalı'
                    }
                    else {
                        delete error['YuzdeOdeme']
                    }
                    teklif.YuzdeOdeme = fiyat
                }
                return {
                    ...state,
                    teklif: teklif/*, tempUrun: tempUrun*/,
                    //seciliUrun: _seciliUrun,
                    error: error
                }
            }
            case Actions.TEKLIF_SET_ILAVE_KOSUL: 
            {
                return {
                    ...state,
                    teklif : {...state.teklif, ilaveKosullar: action.ilaveKosullar}
                };
            }
            case Actions.TEKLIF_SET_IADE: 
            {
                return {
                    ...state,
                    teklif: {...state.teklif, iade: action.iade}
                };
            }
            case Actions.YENI_TEKLIF:
            {
                return {
                    ...state,
                    teklif: {
                        MusteriTemsilcisi: action.userId,
                        teklifTarihi: new Date(),
                        termin: moment(new Date()).add(15, 'days')
                    },
                    error: null,
                    searchText: '',
                }
            }
            case Actions.TEKLIF_HESAPLA:
            {
                let teklif = {...state.teklif}
                teklif.AraToplam =   (teklif.Urun.OnOdemeTutari    || 0) + 
                                    (teklif.Urun.BasariTutari     || 0) + 
                                    (teklif.Urun.SabitTutar       || 0) + 
                                    (teklif.Urun.RaporBasiOdeme   || 0) + 
                                    (teklif.Urun.YuzdeOdeme       || 0);
                
                teklif.Indirim = teklif.AraToplam  -   ((teklif.OnOdemeTutari      || 0) + 
                                                        (teklif.BasariTutari       || 0) + 
                                                        (teklif.SabitTutar         || 0) + 
                                                        (teklif.RaporBasiOdeme     || 0) + 
                                                        (teklif.YuzdeOdeme         || 0) )
                teklif.KDVOrani = teklif.Urun.KDVOrani
                teklif.KDV = (teklif.AraToplam - teklif.Indirim) * (teklif.KDVOrani / 100)
                teklif.Toplam = teklif.AraToplam - teklif.Indirim + teklif.KDV
                return {
                    ...state,
                    teklif : teklif
                }
            }
            case Actions.TEKLIF_SET_SURE:
            {
                return {
                    ...state,
                    teklif: {...state.teklif, sure: action.sure}
                }
            }
            case Actions.TEKLIF_SET_TERMIN:
            {
                return {
                    ...state,
                    teklif: {...state.teklif, termin: action.termin}
                }
            }
        default:
            {
                return state;
            }
    }
};

export default teklifReducer;
