import {combineReducers} from 'redux';
import teklif from './teklif.reducer';
import user from './user.reducer';
import _user from '../../../../auth/store/reducers/user.reducer';

const reducer = combineReducers({
    teklif,
    user,
    _user
});

export default reducer;
