import axios from 'axios';

export const GET_MUSTERILER = '[CONTACTS APP] GET MUSTERILER';

export function getMusteriler()
{
    const request = axios.get('/api/musteri');

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_MUSTERILER,
                payload: response.data
            })
        );
}
