import * as Actions from '../actions';

const initialState = {
    data: null
};

const musterilerReducer = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.GET_MUSTERILER:
            return {
                ...state,
                data: action.payload.data
            };
        default:
            return state;
    }
};

export default musterilerReducer;