import {combineReducers} from 'redux';
import contacts from './contacts.reducer';
import user from './user.reducer';
import musteriler from './musteriler.reducer';

const reducer = combineReducers({
    contacts,
    user,
    musteriler
});

export default reducer;
