import React, { useEffect, useCallback } from 'react';
import { TextField,Select, MenuItem, OutlinedInput, Button, Dialog, DialogActions, DialogContent, Icon, IconButton, Typography, Toolbar, AppBar, Avatar } from '@material-ui/core';
import { useForm } from '@fuse/hooks';
import { FuseLoading } from '@fuse';
import FuseUtils from '@fuse/FuseUtils';
import * as Actions from './store/actions';
import { useDispatch, useSelector } from 'react-redux';

const defaultFormState = {
    id: '',
    Adi: '',
    name: '',
    lastName: '',
    avatar: 'assets/images/avatars/profile.jpg',
    nickname: '',
    company: '',
    jobTitle: '',
    email: '',
    phone: '',
    address: '',
    birthday: '',
    notes: ''
};

function ContactDialog(props) {
    const dispatch = useDispatch();
    const contactDialog = useSelector(({ contactsApp }) => contactsApp.contacts.contactDialog);
    const musteriler = useSelector(({contactsApp}) => contactsApp.musteriler.data);

    const { form, handleChange, setForm } = useForm(defaultFormState);

    const initDialog = useCallback(
        () => {
            /**
             * Dialog type: 'edit'
             */
            if (contactDialog.type === 'edit' && contactDialog.data) {
                setForm({ ...contactDialog.data });
            }

            /**
             * Dialog type: 'new'
             */
            if (contactDialog.type === 'new') {
                setForm({
                    ...defaultFormState,
                    ...contactDialog.data,
                    id: FuseUtils.generateGUID()
                });
            }
        },
        [contactDialog.data, contactDialog.type, setForm],
    );

    useEffect(() => {
        /**
         * After Dialog Open
         */
        dispatch(Actions.getMusteriler());


        if (contactDialog.props.open) {
            initDialog();
        }

    }, [contactDialog.props.open, initDialog]);

    function closeComposeDialog() {
        contactDialog.type === 'edit' ? dispatch(Actions.closeEditContactDialog()) : dispatch(Actions.closeNewContactDialog());
    }

    function canBeSubmitted() {
        return (
            form.Adi.length > 0
        );
    }

    function handleSubmit(event) {
        event.preventDefault();

        if (contactDialog.type === 'new') {
            dispatch(Actions.addContact(form));
        }
        else {
            dispatch(Actions.updateContact(form));
        }
        closeComposeDialog();
    }

    function handleRemove() {
        dispatch(Actions.removeContact(form.id));
        closeComposeDialog();
    }

    if (!musteriler) {
        return <FuseLoading />;
    }


    return (
        <Dialog
            classes={{
                paper: "m-24"
            }}
            {...contactDialog.props}
            onClose={closeComposeDialog}
            fullWidth
            maxWidth="xs"
        >

            <AppBar position="static" elevation={1}>
                <Toolbar className="flex w-full">
                    <Typography variant="subtitle1" color="inherit">
                        {contactDialog.type === 'new' ? 'Yeni Kişi' : 'Kişi Güncelle'}
                    </Typography>
                </Toolbar>
                <div className="flex flex-col items-center justify-center pb-24">
                    <Avatar className="w-96 h-96" alt="contact avatar" src={form.avatar} />
                    {contactDialog.type === 'edit' && (
                        <Typography variant="h6" color="inherit" className="pt-8">
                            {form.Adi}
                        </Typography>
                    )}
                </div>
            </AppBar>
            <form noValidate onSubmit={handleSubmit} className="flex flex-col md:overflow-hidden">
                <DialogContent classes={{ root: "p-24" }}>
                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">account_circle</Icon>
                        </div>

                        <TextField
                            className="mb-24"
                            label="Adı"
                            autoFocus
                            id="Adi"
                            name="Adi"
                            value={form.Adi}
                            onChange={handleChange}
                            variant="outlined"
                            required
                            fullWidth
                        />
                    </div>

                    <div className="flex">
                        <div className="min-w-48 pt-20">
                        </div>
                        <TextField
                            className="mb-24"
                            label="Soyadı"
                            id="Soyadi"
                            name="Soyadi"
                            value={form.Soyadi}
                            onChange={handleChange}
                            variant="outlined"
                            required
                            fullWidth
                        />
                    </div>

                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">star</Icon>
                        </div>
                        <TextField
                            className="mb-24"
                            label="Unvanı"
                            id="Unvani"
                            name="Unvani"
                            value={form.Unvani}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                        />
                    </div>

                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">phone</Icon>
                        </div>
                        <TextField
                            className="mb-24"
                            label="Cep Telefonu"
                            id="CepTel"
                            name="CepTel"
                            value={form.CepTel}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                        />
                    </div>
                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">phone</Icon>
                        </div>
                        <TextField
                            className="mb-24"
                            label="İş Telefonu"
                            id="IsTel"
                            name="IsTel"
                            value={form.IsTel}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                        />
                    </div>
                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">phone</Icon>
                        </div>
                        <TextField
                            className="mb-24"
                            label="Dahili"
                            id="Dahili"
                            name="Dahili"
                            value={form.Dahili}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                        />
                    </div>

                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">email</Icon>
                        </div>
                        <TextField
                            className="mb-24"
                            label="Email"
                            id="Mail"
                            name="Mail"
                            value={form.Mail}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                        />
                    </div>

                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">domain</Icon>
                        </div>
                        <Select
                            fullWidth
                            required
                            labelId="sektor-select-outlined-label"
                            className="mt-8 mb-16"
                            //value={selectedCategory}
                            onChange={handleChange}
                            input={
                                <OutlinedInput
                                    labelWidth={("category".length * 9)}
                                    name="Firma"
                                    id="firma"
                                />
                            }
                        >
                            {
                             musteriler.map(musteri => (
                                <MenuItem value={musteri._id} key={musteri._id}>{musteri.FirmaMarkasi}</MenuItem>
                             ))}
                        </Select>
                    </div>

                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">note</Icon>
                        </div>
                        <TextField
                            className="mb-24"
                            label="Notes"
                            id="notes"
                            name="notes"
                            value={form.notes}
                            onChange={handleChange}
                            variant="outlined"
                            multiline
                            rows={5}
                            fullWidth
                        />
                    </div>
                </DialogContent>

                {contactDialog.type === 'new' ? (
                    <DialogActions className="justify-between p-8">
                        <div className="px-16">
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={handleSubmit}
                                type="submit"
                                disabled={!canBeSubmitted()}
                            >
                                Ekle
                            </Button>
                        </div>
                    </DialogActions>
                ) : (
                        <DialogActions className="justify-between p-8">
                            <div className="px-16">
                                <Button
                                    variant="contained"
                                    color="primary"
                                    type="submit"
                                    onClick={handleSubmit}
                                    disabled={!canBeSubmitted()}
                                >
                                    Kaydet
                            </Button>
                            </div>
                            <IconButton
                                onClick={handleRemove}
                            >
                                <Icon>delete</Icon>
                            </IconButton>
                        </DialogActions>
                    )}
            </form>
        </Dialog>
    );
}

export default ContactDialog;
